#ifndef SCENE_H
#define SCENE_H

#include "dependencies.h"
#include "mesh.h"
#include "cage.h"
#include "deformation.h"

class Scene: public QObject
{
    Q_OBJECT

public:
    enum DisplayMode
    {
        Wireframe,
        Unlit,
        Gouraud,
        Phong,
        Invisible
    };

public:
    Scene(Qt3DExtras::Qt3DWindow* window, Mesh* mesh, Cage* cage, Deformation* deformation);
    ~Scene();

public slots:
    void quit();

    void undoSubdivision();
    void redoSubdivision();

    void changeCutSlideDisplayMode(QAction* action);
    void changeCageDisplayMode(QAction* action);
    void changeOriginalMeshDisplayMode(QAction* action);
    void changeDeformedMeshDisplayMode(QAction* action);

    void loadCube();
    void loadSphere();
    void loadTorus();
    void loadTetrahedronDual();
    void loadOctahedronDual();
    void loadMesh();

    void clearCutSlides();

    void bilinearSubdivision();
    void loopSubdivision();
    void catmullClarkSubdivision();

    void smallNoise();
    void mediumNoise();
    void highNoise();

public slots:
    void setViewportRect(const QRectF& viewportRect);
    void setClearColor(const QColor& clearColor);
    void setCamera(Qt3DCore::QEntity* camera);
    void setSurface(QObject* surface);

    void setCutSlideDisplayMode(DisplayMode displayMode);
    void setCageDisplayMode(DisplayMode displayMode);
    void setOriginalMeshDisplayMode(DisplayMode displayMode);
    void setDeformedMeshDisplayMode(DisplayMode displayMode);

    void mousePressed(Qt3DInput::QMouseEvent* event);
    void mouseReleased(Qt3DInput::QMouseEvent* event);
    void mouseMoved(Qt3DInput::QMouseEvent* event);
    void mouseWheel(Qt3DInput::QWheelEvent* event);

private:
    void extractMesh(Qt3DRender::QGeometryRenderer* renderer);
    void subdivide(Mesh::SubdivisionTechnique);
    void noise(int power, float divisor);
    void regenerateCage();
    void updateCage(const QVector<OsdVec3>& newCoords);
    void updateRenderers();
    void updateGeometry(bool updateCoordinates);
    void changeMaterial(DisplayMode displayMode, Qt3DCore::QEntity* entity, Qt3DRender::QMaterial*& material);

    int grabCageVertex(float x, float y, float grabRadius);
    void rotateCube(float dx, float dy);
    void rotateLightSource(float dx, float dy);
    void moveCube(float dx, float dy);
    void moveCageVertex(float dx, float dy);

    Mesh* m_mesh;
    Cage* m_cage;
    Deformation* m_deformation;
    Geometry* m_displayGeometry;

    float m_meshRotationX, m_meshRotationY;
    float m_lightRotationX, m_lightRotationY;

    enum MouseMode
    {
        MOUSE_MODE_NONE,
        MOUSE_MODE_CUT_SLIDE_PLACING,
        MOUSE_MODE_CAGE_MODIFYING,
        MOUSE_MODE_SCENE_MODIFYING,
    };

    MouseMode m_mouseMode;
    int m_movingCageVertex;
    QVector2D m_cutSlideStart;

    Qt3DExtras::Qt3DWindow* m_window;
    Qt3DRender::QTechniqueFilter* m_techniqueFilter;
    Qt3DRender::QRenderSurfaceSelector* m_surfaceSelector;
    Qt3DRender::QViewport* m_viewport;
    Qt3DRender::QCameraSelector* m_cameraSelector;
    Qt3DRender::QClearBuffers* m_clearBuffer;
    Qt3DRender::QCullFace* m_faceCulling;
    Qt3DRender::QFrustumCulling* m_frustumCulling;
    Qt3DRender::QBlendEquationArguments* m_blendEquations;

    Qt3DCore::QEntity* m_rootEntity;

    Qt3DRender::QCamera* m_cameraEntity;

    Qt3DCore::QEntity* m_controllerEntity;
    Qt3DInput::QMouseDevice* m_mouseDevice;
    Qt3DInput::QMouseHandler* m_mouseHandler;

    Qt3DCore::QTransform* m_transform;

    Qt3DCore::QEntity* m_originalMeshEntity;
    Qt3DRender::QGeometryRenderer* m_originalMeshRenderer;
    Qt3DRender::QMaterial* m_originalMeshMaterial;

    Qt3DCore::QEntity* m_deformedMeshEntity;
    Qt3DRender::QGeometryRenderer* m_deformedMeshRenderer;
    Qt3DRender::QMaterial* m_deformedMeshMaterial;

    Qt3DCore::QEntity* m_cutSlideEntity;
    Qt3DRender::QGeometryRenderer* m_cutSlideRenderer;
    Qt3DRender::QMaterial* m_cutSlideMaterial;

    Qt3DCore::QEntity* m_cageEntity;
    Qt3DRender::QGeometryRenderer* m_cageRenderer;
    Qt3DRender::QMaterial* m_cageMaterial;

    Qt3DCore::QEntity* m_lightEntity;
    Qt3DRender::QDirectionalLight* m_lightSource;

    Qt3DRender::QMaterial* m_wireframeMaterial;
    Qt3DRender::QEffect* m_wireframeEffect;
    Qt3DRender::QTechnique* m_wireframeTechnique;
    Qt3DRender::QFilterKey* m_wireframeFilter;
    Qt3DRender::QRenderPass* m_wireframePass;
    Qt3DRender::QShaderProgram* m_wireframeShader;

    Qt3DRender::QMaterial* m_unlitMaterial;
    Qt3DRender::QEffect* m_unlitEffect;
    Qt3DRender::QTechnique* m_unlitTechnique;
    Qt3DRender::QFilterKey* m_unlitFilter;
    Qt3DRender::QRenderPass* m_unlitPass;
    Qt3DRender::QShaderProgram* m_unlitShader;

    Qt3DRender::QMaterial* m_gouraudMaterial;
    Qt3DRender::QEffect* m_gouraudEffect;
    Qt3DRender::QTechnique* m_gouraudTechnique;
    Qt3DRender::QFilterKey* m_gouraudFilter;
    Qt3DRender::QRenderPass* m_gouraudPass;
    Qt3DRender::QShaderProgram* m_gouraudShader;

    Qt3DRender::QMaterial* m_phongMaterial;
    Qt3DRender::QEffect* m_phongEffect;
    Qt3DRender::QTechnique* m_phongTechnique;
    Qt3DRender::QFilterKey* m_phongFilter;
    Qt3DRender::QRenderPass* m_phongPass;
    Qt3DRender::QShaderProgram* m_phongShader;
};

#endif // SCENE_H
