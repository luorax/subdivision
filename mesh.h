#ifndef MESH_H
#define MESH_H

#include "dependencies.h"
#include "geometry.h"

class Mesh
{
public:
    enum class SubdivisionTechnique
    {
        BILINEAR,
        LOOP,
        CATMARK,
    };

    Mesh();
    ~Mesh();

    void subdivide(SubdivisionTechnique technique);
    void noise(int power, float divisor);

    void setLevel(int level);
    int getLevels() const;
    int getCurrentLevel() const;
    Geometry* getCurrentGeometry() const;

    void extractMesh(Qt3DRender::QGeometryRenderer* geometryRenderer);

private:
    void clearLevels(bool clearAll);

    int m_currentLevel;
    QVector<Geometry*> m_levels;
};

#endif // MESH_H
