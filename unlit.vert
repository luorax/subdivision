#version 330

uniform mat4 mvp;

in vec3 vertexPosition;
in vec4 vertexColor;

out vec4 vertColor;

void main(void)
{
    vertColor = vertexColor;
    gl_Position = mvp * vec4(vertexPosition, 1);
}
