#include "scene.h"

Scene::Scene(Qt3DExtras::Qt3DWindow* window, Mesh* mesh, Cage* cage, Deformation* deformation):
    m_meshRotationX(0.0f),
    m_meshRotationY(0.0f),
    m_lightRotationX(-90.0f),
    m_lightRotationY(0.0f),
    m_mouseMode(MOUSE_MODE_NONE),
    m_movingCageVertex(-1),
    m_cage(cage),
    m_mesh(mesh),
    m_deformation(deformation),
    m_displayGeometry(new Geometry)
{
    // Store the window
    m_window = window;

    // Create the necessary nodes for the window
    m_techniqueFilter = new Qt3DRender::QTechniqueFilter;

    m_surfaceSelector = new Qt3DRender::QRenderSurfaceSelector;
    m_surfaceSelector->setParent(m_techniqueFilter);
    m_surfaceSelector->setSurface(m_window);

    m_viewport = new Qt3DRender::QViewport;
    m_viewport->setParent(m_surfaceSelector);
    m_viewport->setNormalizedRect(QRectF(0.0f, 0.0f, 1.0f, 1.0f));

    m_cameraSelector = new Qt3DRender::QCameraSelector;
    m_cameraSelector->setParent(m_viewport);
    m_cameraSelector->setCamera(m_window->camera());

    m_clearBuffer = new Qt3DRender::QClearBuffers;
    m_clearBuffer->setParent(m_cameraSelector);
    m_clearBuffer->setClearColor(Qt::gray);
    m_clearBuffer->setClearDepthValue(1.0f);
    m_clearBuffer->setBuffers(Qt3DRender::QClearBuffers::ColorDepthBuffer);

    m_faceCulling = new Qt3DRender::QCullFace;
    m_faceCulling->setMode(Qt3DRender::QCullFace::NoCulling);
    m_faceCulling->setParent(m_clearBuffer);

    m_frustumCulling = new Qt3DRender::QFrustumCulling;
    m_frustumCulling->setParent(m_faceCulling);

    m_blendEquations = new Qt3DRender::QBlendEquationArguments;
    m_blendEquations->setSourceRgba(Qt3DRender::QBlendEquationArguments::Source1Alpha);
    m_blendEquations->setSourceRgba(Qt3DRender::QBlendEquationArguments::OneMinusSourceAlpha);
    m_blendEquations->setParent(m_frustumCulling);

    // Create and store the root entity
    m_rootEntity = new Qt3DCore::QEntity;
    m_window->setRootEntity(m_rootEntity);
    m_window->setActiveFrameGraph(m_techniqueFilter);

    // Create the wireframe material
    m_wireframeMaterial = new Qt3DRender::QMaterial;

    // Create the wireframe effect
    m_wireframeEffect = new Qt3DRender::QEffect;
    m_wireframeMaterial->setEffect(m_wireframeEffect);

    // Create the wireframe technique
    m_wireframeTechnique = new Qt3DRender::QTechnique;
    m_wireframeTechnique->graphicsApiFilter()->setApi(Qt3DRender::QGraphicsApiFilter::OpenGL);
    m_wireframeTechnique->graphicsApiFilter()->setMajorVersion(3);
    m_wireframeTechnique->graphicsApiFilter()->setMinorVersion(1);
    m_wireframeTechnique->graphicsApiFilter()->setProfile(Qt3DRender::QGraphicsApiFilter::CoreProfile);
    m_wireframeEffect->addTechnique(m_wireframeTechnique);

    // Create the wireframe filter key
    m_wireframeFilter = new Qt3DRender::QFilterKey;
    m_wireframeFilter->setName(QStringLiteral("renderingStyle"));
    m_wireframeFilter->setValue(QStringLiteral("forward"));
    m_wireframeTechnique->addFilterKey(m_wireframeFilter);

    // Create the wireframe pass
    m_wireframePass = new Qt3DRender::QRenderPass;
    m_wireframeTechnique->addRenderPass(m_wireframePass);

    // Create the wireframe shader
    m_wireframeShader = new Qt3DRender::QShaderProgram;
    m_wireframeShader->setVertexShaderCode(m_wireframeShader->loadSource(QUrl("qrc:/wireframe.vert")));
    m_wireframeShader->setGeometryShaderCode(m_wireframeShader->loadSource(QUrl("qrc:/wireframe.geom")));
    m_wireframeShader->setFragmentShaderCode(m_wireframeShader->loadSource(QUrl("qrc:/wireframe.frag")));

    // Attach the wireframe shader
    m_wireframePass->setShaderProgram(m_wireframeShader);

    // Create the unlit material
    m_unlitMaterial = new Qt3DRender::QMaterial;

    // Create the unlit effect
    m_unlitEffect = new Qt3DRender::QEffect;
    m_unlitMaterial->setEffect(m_unlitEffect);

    // Create the unlit technique
    m_unlitTechnique = new Qt3DRender::QTechnique;
    m_unlitTechnique->graphicsApiFilter()->setApi(Qt3DRender::QGraphicsApiFilter::OpenGL);
    m_unlitTechnique->graphicsApiFilter()->setMajorVersion(3);
    m_unlitTechnique->graphicsApiFilter()->setMinorVersion(1);
    m_unlitTechnique->graphicsApiFilter()->setProfile(Qt3DRender::QGraphicsApiFilter::CoreProfile);
    m_unlitEffect->addTechnique(m_unlitTechnique);

    // Create the unlit filter key
    m_unlitFilter = new Qt3DRender::QFilterKey;
    m_unlitFilter->setName(QStringLiteral("renderingStyle"));
    m_unlitFilter->setValue(QStringLiteral("forward"));
    m_unlitTechnique->addFilterKey(m_unlitFilter);

    // Create the unlit pass
    m_unlitPass = new Qt3DRender::QRenderPass;
    m_unlitTechnique->addRenderPass(m_unlitPass);

    // Create the unlit shader
    m_unlitShader = new Qt3DRender::QShaderProgram;
    m_unlitShader->setVertexShaderCode(m_unlitShader->loadSource(QUrl("qrc:/unlit.vert")));
    m_unlitShader->setFragmentShaderCode(m_unlitShader->loadSource(QUrl("qrc:/unlit.frag")));

    // Attach the unlit shader
    m_unlitPass->setShaderProgram(m_unlitShader);

    // Create the gouraud material
    m_gouraudMaterial = new Qt3DRender::QMaterial;

    // Create the gouraud effect
    m_gouraudEffect = new Qt3DRender::QEffect;
    m_gouraudMaterial->setEffect(m_gouraudEffect);

    // Create the gouraud technique
    m_gouraudTechnique = new Qt3DRender::QTechnique;
    m_gouraudTechnique->graphicsApiFilter()->setApi(Qt3DRender::QGraphicsApiFilter::OpenGL);
    m_gouraudTechnique->graphicsApiFilter()->setMajorVersion(3);
    m_gouraudTechnique->graphicsApiFilter()->setMinorVersion(1);
    m_gouraudTechnique->graphicsApiFilter()->setProfile(Qt3DRender::QGraphicsApiFilter::CoreProfile);
    m_gouraudEffect->addTechnique(m_gouraudTechnique);

    // Create the gouraud filter key
    m_gouraudFilter = new Qt3DRender::QFilterKey;
    m_gouraudFilter->setName(QStringLiteral("renderingStyle"));
    m_gouraudFilter->setValue(QStringLiteral("forward"));
    m_gouraudTechnique->addFilterKey(m_gouraudFilter);

    // Create the gouraud pass
    m_gouraudPass = new Qt3DRender::QRenderPass;
    m_gouraudTechnique->addRenderPass(m_gouraudPass);

    // Create the gouraud shader
    m_gouraudShader = new Qt3DRender::QShaderProgram;
    m_gouraudShader->setVertexShaderCode(m_wireframeShader->loadSource(QUrl("qrc:/gouraud.vert")));
    m_gouraudShader->setFragmentShaderCode(m_wireframeShader->loadSource(QUrl("qrc:/gouraud.frag")));

    // Attach the gouraud shader
    m_gouraudPass->setShaderProgram(m_gouraudShader);

    // Create the phong material
    m_phongMaterial = new Qt3DRender::QMaterial;

    // Create the phong effect
    m_phongEffect = new Qt3DRender::QEffect;
    m_phongMaterial->setEffect(m_phongEffect);

    // Create the phong technique
    m_phongTechnique = new Qt3DRender::QTechnique;
    m_phongTechnique->graphicsApiFilter()->setApi(Qt3DRender::QGraphicsApiFilter::OpenGL);
    m_phongTechnique->graphicsApiFilter()->setMajorVersion(3);
    m_phongTechnique->graphicsApiFilter()->setMinorVersion(1);
    m_phongTechnique->graphicsApiFilter()->setProfile(Qt3DRender::QGraphicsApiFilter::CoreProfile);
    m_phongEffect->addTechnique(m_phongTechnique);

    // Create the phong filter key
    m_phongFilter = new Qt3DRender::QFilterKey;
    m_phongFilter->setName(QStringLiteral("renderingStyle"));
    m_phongFilter->setValue(QStringLiteral("forward"));
    m_phongTechnique->addFilterKey(m_phongFilter);

    // Create the phong pass
    m_phongPass = new Qt3DRender::QRenderPass;
    m_phongTechnique->addRenderPass(m_phongPass);

    // Create the phong shader
    m_phongShader = new Qt3DRender::QShaderProgram;
    m_phongShader->setVertexShaderCode(m_wireframeShader->loadSource(QUrl("qrc:/phong.vert")));
    m_phongShader->setFragmentShaderCode(m_wireframeShader->loadSource(QUrl("qrc:/phong.frag")));

    // Attach the phong shader
    m_phongPass->setShaderProgram(m_phongShader);

    // Camera entity
    m_cameraEntity = m_window->camera();
    m_cameraEntity->lens()->setPerspectiveProjection(60.0f, m_window->size().width() / (float) m_window->size().height(), 0.1f, 100000.0f);
    //m_cameraEntity->lens()->setPerspectiveProjection(60.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    m_cameraEntity->setPosition(QVector3D(0.0f, 0.0f, 20.0f));
    m_cameraEntity->setUpVector(QVector3D(0.0f, 1.0f, 0.0f));
    m_cameraEntity->setViewCenter(QVector3D(0.0f, 0.0f, 0.0f));

    // Camera controller
    m_controllerEntity = new Qt3DCore::QEntity(m_rootEntity);
    m_mouseDevice = new Qt3DInput::QMouseDevice(m_rootEntity);
    m_mouseHandler = new Qt3DInput::QMouseHandler(m_rootEntity);
    m_mouseHandler->setSourceDevice(m_mouseDevice);
    connect(m_mouseHandler, &Qt3DInput::QMouseHandler::pressed, this, &Scene::mousePressed);
    connect(m_mouseHandler, &Qt3DInput::QMouseHandler::released, this, &Scene::mouseReleased);
    connect(m_mouseHandler, &Qt3DInput::QMouseHandler::positionChanged, this, &Scene::mouseMoved);
    connect(m_mouseHandler, &Qt3DInput::QMouseHandler::wheel, this, &Scene::mouseWheel);
    m_controllerEntity->addComponent(m_mouseHandler);

    // Create the light entity
    m_lightEntity = new Qt3DCore::QEntity(m_rootEntity);

    // Create the light component
    m_lightSource = new Qt3DRender::QDirectionalLight(m_rootEntity);
    m_lightEntity->addComponent(m_lightSource);
    m_lightSource->setIntensity(1.0f);
    m_lightSource->setColor(QColor(255, 255, 255));

    // Create the transform component
    m_transform = new Qt3DCore::QTransform(m_rootEntity);
    m_transform->setShareable(true);

    // Create the cut slide entity
    m_cutSlideEntity = new Qt3DCore::QEntity(m_rootEntity);

    // Create the cut slide renderer component
    m_cutSlideRenderer = new Qt3DRender::QGeometryRenderer(m_cutSlideEntity);
    m_cutSlideMaterial = m_wireframeMaterial;

    // Store the cut slide components
    m_cutSlideEntity->addComponent(m_transform);
    m_cutSlideEntity->addComponent(m_cutSlideRenderer);
    m_cutSlideEntity->addComponent(m_cutSlideMaterial);

    // Create the cage entity
    m_cageEntity = new Qt3DCore::QEntity(m_rootEntity);

    // Create the cage renderer component
    m_cageRenderer = new Qt3DRender::QGeometryRenderer(m_cageEntity);
    m_cageMaterial = m_wireframeMaterial;

    // Store the cage components
    m_cageEntity->addComponent(m_transform);
    m_cageEntity->addComponent(m_cageRenderer);
    m_cageEntity->addComponent(m_cageMaterial);

    // Create the mesh entity
    m_originalMeshEntity = new Qt3DCore::QEntity(m_rootEntity);

    // Create the mesh renderer component
    m_originalMeshRenderer = new Qt3DRender::QGeometryRenderer(m_originalMeshEntity);
    m_originalMeshMaterial = nullptr;

    // Store the mesh components
    m_originalMeshEntity->addComponent(m_transform);
    m_originalMeshEntity->addComponent(m_originalMeshRenderer);
    //m_originalMeshEntity->addComponent(m_originalMeshMaterial);

    // Create the mesh entit
    m_deformedMeshEntity = new Qt3DCore::QEntity(m_rootEntity);

    // Create the mesh renderer component
    m_deformedMeshRenderer = new Qt3DRender::QGeometryRenderer(m_deformedMeshEntity);
    m_deformedMeshMaterial = m_phongMaterial;

    // Store the mesh components
    m_deformedMeshEntity->addComponent(m_transform);
    m_deformedMeshEntity->addComponent(m_deformedMeshRenderer);
    m_deformedMeshEntity->addComponent(m_deformedMeshMaterial);

    // Add the dummy cut slides
    m_cage->clearCutSlides();
    m_cage->addCutSlide(
    {

        { 0.0f, -4.2f, 0.0f },
        { 0.0f, -1.0f, 0.0f },
        {
            { 10.0f, 0.0f, 0.0f },
            { 0.0f, 0.0f, 10.0f }
        }
    });
    m_cage->addCutSlide(
    {

        { 0.0f, 4.2f, 0.0f },
        { 0.0f, 1.0f, 0.0f },
        {
            { 10.0f, 0.0f, 0.0f },
            { 0.0f, 0.0f, 10.0f }
        }
    });
}

Scene::~Scene()
{}

void Scene::quit()
{
    QApplication::quit();
}

void Scene::undoSubdivision()
{
    m_mesh->setLevel(m_mesh->getCurrentLevel() - 1);
    regenerateCage();
    updateGeometry(true);
    updateRenderers();
}

void Scene::redoSubdivision()
{
    m_mesh->setLevel(m_mesh->getCurrentLevel() + 1);
    regenerateCage();
    updateGeometry(true);
    updateRenderers();
}

Scene::DisplayMode extractDisplayMode(const QString& displayMode)
{
    if (displayMode == "Gouraud")
    {
        return Scene::Gouraud;
    }
    else if (displayMode == "Phong")
    {
        return Scene::Phong;
    }
    else if (displayMode == "Unlit")
    {
        return Scene::Unlit;
    }
    else if (displayMode == "Wireframe")
    {
        return Scene::Wireframe;
    }
    else if (displayMode == "Invisible")
    {
        return Scene::Invisible;
    }

    return Scene::Wireframe;
}

void Scene::changeCutSlideDisplayMode(QAction* action)
{
    setCutSlideDisplayMode(extractDisplayMode(action->toolTip()));
}

void Scene::changeCageDisplayMode(QAction* action)
{
    setCageDisplayMode(extractDisplayMode(action->toolTip()));
}

void Scene::changeOriginalMeshDisplayMode(QAction* action)
{
    setOriginalMeshDisplayMode(extractDisplayMode(action->toolTip()));
}

void Scene::changeDeformedMeshDisplayMode(QAction* action)
{
    setDeformedMeshDisplayMode(extractDisplayMode(action->toolTip()));
}

void Scene::loadCube()
{
    auto mesh = new Qt3DExtras::QCuboidMesh;
    mesh->setXExtent(4.0f);
    mesh->setYExtent(4.0f);
    mesh->setZExtent(4.0f);
    mesh->setXYMeshResolution(QSize(16, 16));
    mesh->setXZMeshResolution(QSize(16, 16));
    mesh->setYZMeshResolution(QSize(16, 16));

    extractMesh(mesh);
}

void Scene::loadSphere()
{
    auto mesh = new Qt3DExtras::QSphereMesh;
    mesh->setRadius(8.0f);
    mesh->setRings(12);
    mesh->setSlices(12);

    extractMesh(mesh);
}

void Scene::loadTetrahedronDual()
{
    auto mesh = new Qt3DExtras::QSphereMesh;
    mesh->setRadius(8.0f);
    mesh->setRings(2);
    mesh->setSlices(3);

    extractMesh(mesh);
}

void Scene::loadOctahedronDual()
{
    auto mesh = new Qt3DExtras::QSphereMesh;
    mesh->setRadius(8.0f);
    mesh->setRings(2);
    mesh->setSlices(4);

    extractMesh(mesh);
}

void Scene::loadTorus()
{
    auto mesh = new Qt3DExtras::QTorusMesh;
    mesh->setRadius(4.0f);
    mesh->setMinorRadius(2.0f);
    mesh->setRings(100);
    mesh->setSlices(20);

    extractMesh(mesh);
}

void Scene::loadMesh()
{
    QString fileName = QFileDialog::getOpenFileName(
        nullptr,
        QStringLiteral("Open Mesh"),
        QDir::currentPath() + "/mesh/",
        //QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).front(),
        QStringLiteral("Mesh Files (*.obj *.x)"));

    if (fileName.isNull())
        return;

    auto mesh = new Qt3DRender::QMesh;
    mesh->setSource(QUrl::fromLocalFile(fileName));

    extractMesh(mesh);
}

void Scene::clearCutSlides()
{
    m_cage->clearCutSlides();
    regenerateCage();
    updateGeometry(true);
    updateRenderers();
}

void Scene::bilinearSubdivision()
{
    subdivide(Mesh::SubdivisionTechnique::BILINEAR);
}

void Scene::loopSubdivision()
{
    subdivide(Mesh::SubdivisionTechnique::LOOP);
}

void Scene::catmullClarkSubdivision()
{
    subdivide(Mesh::SubdivisionTechnique::CATMARK);
}

void Scene::smallNoise()
{
    noise(4, 32.0f);
}

void Scene::mediumNoise()
{
    noise(2, 16.0f);
}

void Scene::highNoise()
{
    noise(1, 8.0f);
}

void Scene::setViewportRect(const QRectF& viewportRect)
{
    m_viewport->setNormalizedRect(viewportRect);
}

void Scene::setClearColor(const QColor& clearColor)
{
    m_clearBuffer->setClearColor(clearColor);
}

void Scene::setCamera(Qt3DCore::QEntity* camera)
{
    m_cameraSelector->setCamera(camera);
}

void Scene::setSurface(QObject* surface)
{
    m_surfaceSelector->setSurface(surface);
}

void Scene::changeMaterial(DisplayMode displayMode, Qt3DCore::QEntity* entity, Qt3DRender::QMaterial*& material)
{
    switch (displayMode)
    {
    case Gouraud:
        if (material) entity->removeComponent(material);
        material = m_gouraudMaterial;
        entity->addComponent(material);
        break;

    case Phong:
        if (material) entity->removeComponent(material);
        material = m_phongMaterial;
        entity->addComponent(material);
        break;

    case Unlit:
        if (material) entity->removeComponent(material);
        material = m_unlitMaterial;
        entity->addComponent(material);
        break;

    case Wireframe:
        if (material) entity->removeComponent(material);
        material = m_wireframeMaterial;
        entity->addComponent(material);
        break;

    case Invisible:
        entity->removeComponent(material);
        break;
    };
}

void Scene::setCutSlideDisplayMode(DisplayMode displayMode)
{
    changeMaterial(displayMode, m_cutSlideEntity, m_cutSlideMaterial);
}

void Scene::setCageDisplayMode(DisplayMode displayMode)
{
    changeMaterial(displayMode, m_cageEntity, m_cageMaterial);
}

void Scene::setOriginalMeshDisplayMode(DisplayMode displayMode)
{
    changeMaterial(displayMode, m_originalMeshEntity, m_originalMeshMaterial);
}

void Scene::setDeformedMeshDisplayMode(DisplayMode displayMode)
{
    changeMaterial(displayMode, m_deformedMeshEntity, m_deformedMeshMaterial);
}

void Scene::extractMesh(Qt3DRender::QGeometryRenderer* renderer)
{
    m_mesh->extractMesh(renderer);
    delete renderer;

    regenerateCage();
    updateGeometry(true);
    updateRenderers();
}

void Scene::subdivide(Mesh::SubdivisionTechnique technique)
{
    m_mesh->subdivide(technique);
    regenerateCage();
    updateGeometry(true);
    updateRenderers();
}

void Scene::noise(int power, float divisor)
{
    m_mesh->noise(power, divisor);
    regenerateCage();
    updateGeometry(true);
    updateRenderers();
}

void loadCubeCage(Cage* cage)
{
    auto mesh = new Qt3DExtras::QCuboidMesh;
    mesh->setXExtent(20.0f);
    mesh->setYExtent(20.0f);
    mesh->setZExtent(20.0f);
    mesh->setXYMeshResolution(QSize(4, 4));
    mesh->setXZMeshResolution(QSize(4, 4));
    mesh->setYZMeshResolution(QSize(4, 4));

    // Extract the geometry we are copying
    Qt3DRender::QGeometry* qgeom = mesh->geometry();
    if (qgeom == nullptr)
        qgeom = (*mesh->geometryFactory())();

    Geometry* geom = new Geometry(qgeom);
    cage->getCageGeometry()->setPositions(geom->getPositions());
    cage->getCageGeometry()->setIndices(geom->getIndices());
    cage->getCageGeometry()->setColors(cage->getCageGeometry()->getColors()[0]);
    cage->getCageGeometry()->filterVertices();
    cage->getCageGeometry()->generateNormals();
    cage->getCageGeometry()->upload();

    cage->getCutSlideGeometry()->setPositions({});
    cage->getCutSlideGeometry()->setIndices({});
    cage->getCutSlideGeometry()->upload();

    delete geom;
    delete mesh;
}

void Scene::regenerateCage()
{
    m_cage->generateGeometry(m_mesh->getCurrentGeometry());
    m_cage->getCutSlideGeometry()->setColors(QColor(255, 0, 0, 127));
    m_cage->getCutSlideGeometry()->upload();
    m_cage->getCageGeometry()->setColors(QColor(0, 255, 0, 127));
    m_cage->getCageGeometry()->upload();

    //loadCubeCage(m_cage);
}

void Scene::updateCage(const QVector<OsdVec3>& newCoords)
{
    m_deformation->updateCoordinates(m_cage->getCageGeometry()->getPositions(), newCoords);
    m_cage->getCageGeometry()->setPositions(newCoords);
    m_cage->getCutSlideGeometry()->generateNormals();
    m_cage->getCutSlideGeometry()->setColors(QColor(255, 0, 0, 127));
    m_cage->getCutSlideGeometry()->upload();
    m_cage->getCageGeometry()->generateNormals();
    m_cage->getCageGeometry()->setColors(QColor(0, 255, 0, 127));
    m_cage->getCageGeometry()->upload();
}

void Scene::updateRenderers()
{
    m_originalMeshRenderer->setGeometry(m_mesh->getCurrentGeometry()->getGeometry());
    m_deformedMeshRenderer->setGeometry(m_displayGeometry->getGeometry());
    m_cageRenderer->setGeometry(m_cage->getCageGeometry()->getGeometry());
    m_cutSlideRenderer->setGeometry(m_cage->getCutSlideGeometry()->getGeometry());
}

void Scene::updateGeometry(bool updateCoordinates)
{
    m_deformation->setCage(m_cage);
    m_deformation->setSource(m_mesh->getCurrentGeometry());
    m_deformation->setTarget(m_displayGeometry);
    if (updateCoordinates)
    {
        m_deformation->computeCoordinates();
    }
    m_deformation->deformGeometry();
}

int Scene::grabCageVertex(float x, float y, float grabRadius)
{
    // Extract the relevant matrices
    QMatrix4x4 model = m_transform->matrix();
    QMatrix4x4 view = m_window->camera()->viewMatrix();
    QMatrix4x4 projection = m_window->camera()->projectionMatrix();
    QMatrix4x4 mvp = projection * view * model;

    // Extract the cage vertices
    const auto& cagePositions = m_cage->getCageGeometry()->getPositions();

    // Go through the cage vertices
    int closestIndex = -1;
    float closestZ = 90000.0f;
    QVector2D posMouse(x, m_window->size().height() - y - 1);

    for (int i = 0; i < cagePositions.size(); ++i)
    {
        // Project it onto the screen
        QVector4D position(cagePositions[i].position[0], cagePositions[i].position[1], cagePositions[i].position[2], 1.0f);
        QVector4D posNDC = mvp * position;
        posNDC = posNDC / posNDC.w();

        // Compute the window coordinates
        QVector2D posScreen(
                    (posNDC.x() * 0.5f + 0.5f) * m_window->size().width(),
                    (posNDC.y() * 0.5f + 0.5f) * m_window->height());

        // Compute the distance to the mouse cursor
        float dist = posScreen.distanceToPoint(posMouse);
        if (dist < grabRadius && posNDC.z() < closestZ)
        {
            closestZ = posNDC.z();
            closestIndex = i;
        }
    }

    return closestIndex;
}

void Scene::mousePressed(Qt3DInput::QMouseEvent* event)
{
    if ((event->modifiers() & Qt::ShiftModifier) != 0)
    {
        m_cutSlideStart = QVector2D(event->x(), event->y());
        m_mouseMode = MOUSE_MODE_CUT_SLIDE_PLACING;
    }
    else if ((event->modifiers() & Qt::ControlModifier) != 0)
    {
        m_movingCageVertex = grabCageVertex(event->x(), event->y(), 20.0f);
        m_mouseMode = MOUSE_MODE_CAGE_MODIFYING;
    }
    else
    {
        m_mouseMode = MOUSE_MODE_SCENE_MODIFYING;
    }
}

void Scene::mouseReleased(Qt3DInput::QMouseEvent* event)
{
    if (m_mouseMode == MOUSE_MODE_CUT_SLIDE_PLACING)
    {
        m_cage->addCutSlide(m_cutSlideStart, QVector2D(event->x(), event->y()));
        regenerateCage();
        updateGeometry(true);
        updateRenderers();
    }

    m_mouseMode = MOUSE_MODE_NONE;
}

void Scene::rotateCube(float dx, float dy)
{
    m_meshRotationX += dy / 5.0;
    m_meshRotationY += dx / 5.0;

    m_transform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), m_meshRotationX) *
        QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), m_meshRotationY));
}

void Scene::rotateLightSource(float dx, float dy)
{
    m_lightRotationX += dy / 5.0;
    m_lightRotationY += dx / 5.0;

    auto rotation = QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), m_lightRotationX) *
            QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), m_lightRotationY);
    auto direction = (rotation * QVector3D(0, 0, -1)).normalized();

    m_lightSource->setWorldDirection(direction);
}

void Scene::moveCube(float dx, float dy)
{
    auto delta = QVector3D(dx / 500.0f, -dy / 500.0f, 0.0f) * m_cameraEntity->position().z();
    m_transform->setTranslation(m_transform->translation() + delta);
}

void Scene::moveCageVertex(float dx, float dy)
{
    // Delta factors
    float deltaX = dx / 50.0f;
    float deltaY = -dy / 50.0f;

    // Extract the positions
    auto positions = m_cage->getCageGeometry()->getPositions();
    auto& movedPos = positions[m_movingCageVertex];

    // Extract the current right and up directions
    QMatrix4x4 model = m_transform->matrix();
    QMatrix4x4 modelInverse = model.inverted();
    QMatrix4x4 view = m_window->camera()->viewMatrix();
    QMatrix4x4 viewInverse = view.inverted();
    QVector4D right = view * QVector4D(1.0f, 0.0f, 0.0f, 0.0f);
    QVector4D up = view * QVector4D(0.0f, 1.0f, 0.0f, 0.0f);

    // Compute the inverse transforms
    QVector4D deltaPosition = right * deltaX + up * deltaY;
    QVector4D delta = modelInverse * viewInverse * deltaPosition;

    // Update the moved vertex
    movedPos.position[0] += delta.x();
    movedPos.position[1] += delta.y();
    movedPos.position[2] += delta.z();

    // Update the geometry
    updateCage(positions);
    updateGeometry(false);
    updateRenderers();
}

void Scene::mouseMoved(Qt3DInput::QMouseEvent* event)
{
    // Mouse coordinates
    static int s_x = -1, s_y = -1;

    // Delta coordinates
    int dx = event->x() - s_x, dy = event->y() - s_y;

    // Whether this is the first call or not
    bool firstCall = s_x == -1 && s_y == -1;

    // Store the new mouse coordinates
    s_x = event->x();
    s_y = event->y();

    // Skip any logic if this was the first call
    if (firstCall) return;

    // Are we modifying the scene?
    if (m_mouseMode == MOUSE_MODE_SCENE_MODIFYING)
    {
        // rotate the cube
        if ((event->buttons() & Qt3DInput::QMouseEvent::LeftButton) != 0)
        {
            rotateCube(dx, dy);
        }

        // rotate the light source
        else if ((event->buttons() & Qt3DInput::QMouseEvent::RightButton) != 0)
        {
            rotateLightSource(dx, dy);
        }

        // translate the cube
        else if ((event->buttons() & Qt3DInput::QMouseEvent::MiddleButton) != 0)
        {
            moveCube(dx, dy);
        }
    }

    // Are we modifying the cage?
    else if (m_mouseMode == MOUSE_MODE_CAGE_MODIFYING)
    {
        if (m_movingCageVertex != -1)
        {
            moveCageVertex(dx, dy);
        }
    }
}

void Scene::mouseWheel(Qt3DInput::QWheelEvent* event)
{
    auto degrees = (event->angleDelta().y() / 8.0) / 225;
    auto position = m_cameraEntity->position();
    auto multiplier = QVector3D(1, 1, (degrees > 0) ? (1.0f - degrees) : 1.0f / (1.0f + degrees));

    m_cameraEntity->setPosition(position * multiplier);
}
