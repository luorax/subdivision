#-------------------------------------------------
#
# Project created by QtCreator 2016-11-14T10:25:51
#
#-------------------------------------------------

QT += 3dcore 3drender 3dinput 3dextras
QT += core gui widgets

TARGET = Subdivision
TEMPLATE = app

CONFIG += precompile_header
PRECOMPILED_HEADER = dependencies.h

SOURCES += \
    main.cpp\
    scene.cpp \
    mesh.cpp \
    cage.cpp \
    geometry.cpp \
    osdvec3.cpp \
    deformation.cpp

HEADERS  += \
    scene.h \
    dependencies.h \
    mesh.h \
    cage.h \
    geometry.h \
    osdvec3.h \
    deformation.h

QMAKE_CXXFLAGS += -bigobj

INCLUDEPATH  += $$PWD/dependencies/opensubdiv/
CONFIG(debug, debug|release): LIBS += -L$$PWD/dependencies/opensubdiv/lib/ -losdCPU-debug
CONFIG(release, debug|release): LIBS += -L$$PWD/dependencies/opensubdiv/lib/ -losdCPU-release

INCLUDEPATH  += $$PWD/dependencies/boost/

INCLUDEPATH  += $$PWD/dependencies/gmp/gmp/
LIBS += -L$$PWD/dependencies/gmp/lib/ -llibgmp-10 -llibmpfr-4

INCLUDEPATH  += $$PWD/dependencies/CGAL/
CONFIG(debug, debug|release): LIBS += -L$$PWD/dependencies/CGAL/lib/ -lCGAL_Core-vc140-mt-gd-4.10 -lCGAL-vc140-mt-gd-4.10
CONFIG(release, debug|release): LIBS += -L$$PWD/dependencies/CGAL/lib/ -lCGAL_Core-vc140-mt-4.10 -lCGAL-vc140-mt-4.10

INCLUDEPATH  += $$PWD/dependencies/Eigen/

INCLUDEPATH  += $$PWD/dependencies/igl/

INCLUDEPATH  += $$PWD/dependencies/miniglog/

INCLUDEPATH  += $$PWD/dependencies/ceres/
CONFIG(debug, debug|release): LIBS += -L$$PWD/dependencies/ceres/lib/ -lceres-debug
CONFIG(release, debug|release): LIBS += -L$$PWD/dependencies/ceres/lib/ -lceres-release

RESOURCES += \
    shaders.qrc
