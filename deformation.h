#ifndef DEFORMATION_H
#define DEFORMATION_H

#include "dependencies.h"
#include "cage.h"
#include "geometry.h"

struct GreenCoordinate
{
    QVector<float> m_positions;
    QVector<float> m_normals;
};

class Deformation
{
public:
    Deformation();
    Deformation(Cage* cage, Geometry* source, Geometry* target);

    void computeCoordinates();
    void updateCoordinates(const QVector<OsdVec3>& oldCage, const QVector<OsdVec3>& newCage);
    void deformGeometry();

    void setSource(Geometry* geom);
    void setTarget(Geometry* geom);
    void setCage(Cage* cage);

    Geometry* getSource() const;
    Geometry* getTarget() const;
    Cage* getCage() const;

private:
    Cage* m_cage;
    Geometry* m_source;
    Geometry* m_target;
    QVector<GreenCoordinate> m_weights;
};

#endif // DEFORMATION_H
