#ifndef CAGE_H
#define CAGE_H

#include "dependencies.h"
#include "geometry.h"

struct CutSlide
{
    OsdVec3 m_center;
    OsdVec3 m_normal;
    OsdVec3 m_radi[2];
};

class Cage
{
public:
    Cage();

    void addCutSlide(QVector2D start, QVector2D end);
    void addCutSlide(CutSlide cutSlide);
    void clearCutSlides();

    void generateGeometry(Geometry* base);

    Geometry* getCageGeometry() const;
    Geometry* getCutSlideGeometry() const;

private:
    void generateCutSlideGeometry();
    void generateCageGeometry(Geometry* base);

    Geometry* m_cutSlideGeometry;
    Geometry* m_cageGeometry;

    QVector<CutSlide> m_cutSlidesOriginal;
    QVector<CutSlide> m_cutSlidesOptimal;
};

#endif // CAGE_H
