#version 330

uniform mat4 mvp;
uniform mat4 modelMatrix;
uniform mat3 modelNormalMatrix;

uniform vec3 eyePosition;

const int MAX_LIGHTS = 8;
const int TYPE_POINT = 0;
const int TYPE_DIRECTIONAL = 1;
const int TYPE_SPOT = 2;
struct Light
{
    int type;
    vec3 position;
    vec3 color;
    float intensity;
    vec3 direction;
    vec3 attenuation;
    float cutOffAngle;
};

uniform Light lights[MAX_LIGHTS];
uniform int lightCount;

in vec3 vertexPosition;
in vec3 vertexNormal;
in vec4 vertexColor;

out vec3 lightColor;
out vec4 vertColor;

vec3 calcLight(vec3 position, vec3 normal)
{
    // Accumulated values
    vec3 diffuseColor = vec3(0.0);
    vec3 specularColor = vec3(0.0);

    // Go through each light source and process it
    for (int i = 0; i < lightCount; ++i)
    {
        // Direction of the light
        vec3 incidence = normalize(lights[i].direction);

        // Computed diffuse intensity
        float diffuseIntensity = max(dot(-incidence, normal), 0.0);

        // Accumulate the computed diffuse intensity
        diffuseColor += diffuseIntensity * lights[i].intensity * lights[i].color;

        // Vector pointing from the vertex to the eye position
        vec3 toEye = normalize(eyePosition - position);

        // Incident light direction reflected by the surface normal
        vec3 reflected = reflect(incidence, normal);

        // Computed specular intensity
        float specularIntensity = pow(max(dot(toEye, reflected), 0.0), 255);

        // Accumulate the computed specular intensity
        specularColor += specularIntensity * lights[i].intensity * lights[i].color;
    }

    // Return the accumulated diffuse color
    return diffuseColor + specularColor;
}

void main(void)
{
    vec3 position = vec3(modelMatrix * vec4(vertexPosition, 1));
    vec3 normal = normalize(modelNormalMatrix * vertexNormal);

    lightColor = calcLight(position, normal);
    vertColor = vertexColor;
    gl_Position = mvp * vec4(vertexPosition, 1);
}
