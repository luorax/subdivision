#ifndef OSDVEC3_H
#define OSDVEC3_H

#include "dependencies.h"

struct OsdVec3
{
    OsdVec3();
    OsdVec3(float a, float b, float c);
    OsdVec3(const OsdVec3& src);

    void Clear();
    void AddWithWeight(const OsdVec3& src, float weight);
    void SetPosition(float x, float y, float z);
    const float* GetPosition() const;

    OsdVec3 operator+(const OsdVec3& rhs) const;
    OsdVec3 operator-(const OsdVec3& rhs) const;
    OsdVec3 operator*(const OsdVec3& rhs) const;
    OsdVec3 operator/(const OsdVec3& rhs) const;
    OsdVec3 operator+(float rhs) const;
    OsdVec3 operator-(float rhs) const;
    OsdVec3 operator*(float rhs) const;
    OsdVec3 operator/(float rhs) const;

    float position[3];
};

// Returns the length of \p v.
float length(OsdVec3 v);

// Returns the normalized version of \p v.
OsdVec3 normalize(OsdVec3 v);

// Returns the cross product of \p v1 and \p v2.
OsdVec3 cross(OsdVec3 v1, OsdVec3 v2);

// Returns the dot product of \p v1 and \p v2.
float dot(OsdVec3 v1, OsdVec3 v2);

// Returns the distance between of \p v1 and \p v2.
float distance(OsdVec3 v1, OsdVec3 v2);

OsdVec3 vecMin(OsdVec3 v1, OsdVec3 v2);

OsdVec3 vecMax(OsdVec3 v1, OsdVec3 v2);

float compMin(OsdVec3 v);

float compMax(OsdVec3 v);

float extent(OsdVec3 v1, OsdVec3 v2);

float randFloat(float min, float max);

#endif // OSDVEC3_H
