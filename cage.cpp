#include "cage.h"

#include "igl/copyleft/cgal/intersect_other.h"
#include "igl/facet_components.h"
#include "igl/triangle_triangle_adjacency.h"
#include "igl/vertex_triangle_adjacency.h"
#include "igl/centroid.h"

#include "CGAL/Exact_predicates_exact_constructions_kernel.h"
#include "CGAL/Triangulation_vertex_base_3.h"
#include "CGAL/Triangulation_vertex_base_with_info_3.h"
#include "CGAL/Delaunay_triangulation_3.h"

#undef ERROR
#include "ceres/ceres.h"

Cage::Cage():
    m_cutSlideGeometry(nullptr),
    m_cageGeometry(nullptr)
{}

void Cage::addCutSlide(QVector2D start, QVector2D end)
{
    qDebug() << start << end;
}

void Cage::addCutSlide(CutSlide cutSlide)
{
    m_cutSlidesOriginal.push_back(cutSlide);
}

void Cage::clearCutSlides()
{
    m_cutSlidesOriginal.clear();
    m_cutSlidesOptimal.clear();
}

Geometry* Cage::getCageGeometry() const
{
    return m_cageGeometry;
}

Geometry* Cage::getCutSlideGeometry() const
{
    return m_cutSlideGeometry;
}

void Cage::generateGeometry(Geometry* base)
{
    generateCutSlideGeometry();
    generateCageGeometry(base);
}

OsdVec3 cutSlideVertex(const CutSlide& cutSlide, int cutSlideIndex, int vertexIndex)
{

    // Base offset weights
    static const float BASE_OFFSETS[4][2] =
    {
        { -1.0f, -1.0f },
        {  1.0f, -1.0f },
        {  1.0f,  1.0f },
        { -1.0f,  1.0f },
    };

    return cutSlide.m_center +
            cutSlide.m_radi[0] * BASE_OFFSETS[vertexIndex][0] +
            cutSlide.m_radi[1] * BASE_OFFSETS[vertexIndex][1];
}

quint32 cutSlideIndex(const CutSlide& cutSlide, int cutSlideIndex, int indexIndex)
{
    // Base indices
    static const quint32 BASE_INDICES[6] =
    {
        0, 1, 2,
        0, 2, 3
    };

    return cutSlideIndex * 4 + BASE_INDICES[indexIndex];
}

void Cage::generateCutSlideGeometry()
{
    // Delete any existing geometry
    if (m_cutSlideGeometry != nullptr)
    {
        delete m_cutSlideGeometry;
    }

    // Generate vertex data
    QVector<OsdVec3> cutSlidePositionsGeometry(m_cutSlidesOriginal.size() * 4);
    QVector<quint32> cutSlideIndicesGeometry(m_cutSlidesOriginal.size() * 6);

    for (int i = 0; i < m_cutSlidesOriginal.size(); ++i)
    {
        // Base offset weights
        float BASE_OFFSETS[4][2] =
        {
            { -1.0f, -1.0f },
            {  1.0f, -1.0f },
            {  1.0f,  1.0f },
            { -1.0f,  1.0f },
        };

        // Generate vertices
        for (int j = 0; j < 4; ++j)
        {
            cutSlidePositionsGeometry[i * 4 + j] = cutSlideVertex(m_cutSlidesOriginal[i], i, j);
        }

        // Base indices
        static const quint32 BASE_INDICES[6] =
        {
            0, 1, 2,
            0, 2, 3
        };

        // Generate indices
        for (int j = 0; j < 6; ++j)
        {
            cutSlideIndicesGeometry[i * 6 + j] = cutSlideIndex(m_cutSlidesOriginal[i], i, j);
        }
    }

    // Upload to the GPU
    m_cutSlideGeometry = new Geometry(cutSlidePositionsGeometry, cutSlideIndicesGeometry, 0.0f, 9000.0f);
}

struct RefinementCostFunction
{
    RefinementCostFunction(OsdVec3 sp, const QVector<OsdVec3>& normals, const QVector<OsdVec3>& anchors, const std::vector<int>& triangles):
        startingPoint(sp),
        cageNormals(normals),
        cageAnchors(anchors),
        incidentTriangles(triangles)
    {}

    template<typename T>
    bool operator()(const T* x0, T* residuals) const
    {
        T cost = T(0);

        for (int i = 0; i < incidentTriangles.size(); ++i)
        {
            OsdVec3 normal = cageNormals[incidentTriangles[i]];
            OsdVec3 anchor = cageAnchors[incidentTriangles[i]];

            T d = T(0);
            for (int j = 0; j < 3; ++j)
            {
                T t = (x0[j] - T(anchor.position[j])) * T(normal.position[j]);
                d = d + t;
            }

            T dividend = d * d;
            T divider = T(dot(startingPoint - anchor, startingPoint - anchor));

            cost += dividend / divider;
        }

        residuals[0] = cost;
        return true;
    }

    OsdVec3 startingPoint;
    const QVector<OsdVec3>& cageNormals;
    const QVector<OsdVec3>& cageAnchors;
    const std::vector<int>& incidentTriangles;
};

void Cage::generateCageGeometry(Geometry* base)
{
    // Delete any existing geometry
    if (m_cageGeometry != nullptr)
    {
        delete m_cageGeometry;
    }

    bool success = true;

    // Convert the mesh into igl's representation
    const auto& meshVerticesRaw = base->getPositions();
    Eigen::MatrixXd meshVertices(meshVerticesRaw.size(), 3);
    for (int i = 0; i < meshVerticesRaw.size(); ++i)
    {
        meshVertices.row(i) = Eigen::RowVector3d(
                meshVerticesRaw[i].position[0],
                meshVerticesRaw[i].position[1],
                meshVerticesRaw[i].position[2]);
    }

    const auto& meshIndicesRaw = base->getIndices();
    Eigen::MatrixXi meshIndices(meshIndicesRaw.size() / 3, 3);
    for (int i = 0; i < meshIndicesRaw.size() / 3; ++i)
    {
        meshIndices.row(i) = Eigen::RowVector3i(
                meshIndicesRaw[i * 3 + 0],
                meshIndicesRaw[i * 3 + 1],
                meshIndicesRaw[i * 3 + 2]);
    }

    // Also convert the cut slides into igl's representation
    QVector<Eigen::MatrixXd> cutSlideVertices(m_cutSlidesOriginal.size(), Eigen::MatrixXd(4, 3));
    QVector<Eigen::MatrixXi> cutSlideIndices(m_cutSlidesOriginal.size(), Eigen::MatrixXi(2, 3));

    for (int i = 0; i < m_cutSlidesOriginal.size(); ++i)
    {
        // Generate vertices
        for (int j = 0; j < 4; ++j)
        {
            OsdVec3 vertex = cutSlideVertex(m_cutSlidesOriginal[i], 0, j);

            cutSlideVertices[i].row(j) = Eigen::RowVector3d(
                        vertex.position[0],
                        vertex.position[1],
                        vertex.position[2]);
        }

        // Generate indices
        for (int j = 0; j < 2; ++j)
        {
            cutSlideIndices[i].row(j) = Eigen::RowVector3i(
                    cutSlideIndex(m_cutSlidesOriginal[i], 0, j * 3 + 0),
                    cutSlideIndex(m_cutSlidesOriginal[i], 0, j * 3 + 1),
                    cutSlideIndex(m_cutSlidesOriginal[i], 0, j * 3 + 2));
        }
    }

    // Find the mesh-cut slide intersections
    QVector<Eigen::MatrixXi> intersections(m_cutSlidesOriginal.size());
    QVector<Eigen::MatrixXi> intersectionIndices(m_cutSlidesOriginal.size());
    for (int i = 0; i < m_cutSlidesOriginal.size(); ++i)
    {
        igl::copyleft::cgal::intersect_other(meshVertices, meshIndices, cutSlideVertices[i], cutSlideIndices[i], false, intersections[i]);

        intersectionIndices[i] = Eigen::MatrixXi(intersections[i].rows(), 3);

        // Remove the intersecting triangles
        int uniqueTriangles = 0;
        for (int j = 0; j < intersections[i].rows(); ++j)
        {
            int triangleId = intersections[i](j, 0);
            if (meshIndices(triangleId, 0) != 0 || meshIndices(triangleId, 1) != 0 || meshIndices(triangleId, 2) != 0)
            {
                intersectionIndices[i].row(uniqueTriangles++) = meshIndices.row(triangleId);
                meshIndices.row(triangleId) = Eigen::RowVector3i(0, 0, 0);
            }
        }
        intersectionIndices[i].conservativeResize(uniqueTriangles, 3);

        int remainingRows = meshIndices.rows();
        for (int j = 0; j < remainingRows; )
        {
            if (meshIndices(j, 0) == 0 && meshIndices(j, 1) == 0 && meshIndices(j, 2) == 0)
            {
                meshIndices.row(j) = meshIndices.row(remainingRows - 1);
                --remainingRows;
            }
            else
            {
                ++j;
            }
        }
        meshIndices.conservativeResize(remainingRows, 3);
    }

    std::vector<std::vector<std::vector<int>>> TT;
    std::vector<std::vector<std::vector<int>>> TTi;
    Eigen::VectorXi componentMap;
    Eigen::VectorXi componentCounts;

    igl::triangle_triangle_adjacency(meshIndices, TT, TTi);
    igl::facet_components(TT, componentMap, componentCounts);

    // Extract their indices
    QVector<Eigen::MatrixXi> componentIndices(componentCounts.rows());

    for (int i = 0; i < componentIndices.size(); ++i)
    {
        componentIndices[i] = Eigen::MatrixXi(componentCounts[i], 3);
    }

    for (int i = 0; i < componentMap.rows(); ++i)
    {
        int componentId = componentMap[i];
        int rowId = componentCounts[componentId] - 1;

        componentCounts[componentId] = componentCounts[componentId] - 1;
        componentIndices[componentId].row(rowId) = meshIndices.row(i);
    }

    // Assign bounding cut slides to components
    QVector<QVector<int>> componentCutSlides(componentCounts.rows());
    for (int i = 0; i < componentIndices.size(); ++i)
    {
        for (int j = 0; j < m_cutSlidesOriginal.size(); ++j)
        {
            Eigen::MatrixXi combinedIndices(componentIndices[i].rows() + intersectionIndices[j].rows(), 3);

            combinedIndices.block(0, 0, componentIndices[i].rows(), 3) = componentIndices[i];
            combinedIndices.block(componentIndices[i].rows(), 0, intersectionIndices[j].rows(), 3) = intersectionIndices[j];

            std::vector<std::vector<std::vector<int>>> TT;
            std::vector<std::vector<std::vector<int>>> TTi;
            Eigen::VectorXi componentMap;
            Eigen::VectorXi componentCounts;

            igl::triangle_triangle_adjacency(combinedIndices, TT, TTi);
            igl::facet_components(TT, componentMap, componentCounts);

            if (componentCounts.rows() == 1)
            {
                componentCutSlides[i].push_back(j);
            }
        }
    }

    // Compute the optimal cut slide placement
    m_cutSlidesOptimal = m_cutSlidesOriginal;

    // Append the rectangular caps
    for (int i = 0; i < componentCutSlides.size(); ++i)
    {
        if (componentCutSlides[i].size() == 1)
        {
            Eigen::Vector3d centroid;
            igl::centroid(meshVertices, componentIndices[i], centroid);

            OsdVec3 componentCenter(centroid[0], centroid[1], centroid[2]);

            CutSlide cutSlide = m_cutSlidesOptimal[componentCutSlides[i][0]];
            cutSlide.m_center = componentCenter;

            componentCutSlides[i].push_back(m_cutSlidesOptimal.size());
            m_cutSlidesOptimal.push_back(cutSlide);
        }

        std::sort(componentCutSlides[i].begin(), componentCutSlides[i].end());
    }

    // Compute the starting cage by using triangulation
    typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
    typedef CGAL::Triangulation_vertex_base_with_info_3<quint32, Kernel> VertexBase;
    typedef CGAL::Triangulation_cell_base_3<Kernel> CellBase;
    typedef CGAL::Triangulation_data_structure_3<VertexBase, CellBase> TriangulationDS;
    typedef CGAL::Delaunay_triangulation_3<Kernel, TriangulationDS> Triangulation;
    typedef Triangulation::Point TriangulationPoint;

    std::vector<std::pair<TriangulationPoint, quint32>> cagePoints(m_cutSlidesOptimal.size() * 4);

    for (int i = 0; i < m_cutSlidesOptimal.size(); ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            OsdVec3 vertex = cutSlideVertex(m_cutSlidesOptimal[i], 0, j);
            TriangulationPoint point = TriangulationPoint(vertex.position[0], vertex.position[1], vertex.position[2]);
            cagePoints[i * 4 + j] = std::make_pair(point, i * 4 + j);
        }
    }

    Triangulation triangulation(cagePoints.begin(), cagePoints.end());

    QVector<OsdVec3> rawCagePositions;
    QVector<OsdVec3> rawCageNormals;
    QVector<quint32> rawCageIndices;

    rawCagePositions.resize(cagePoints.size());
    for (int i = 0; i < cagePoints.size(); ++i)
    {
        rawCagePositions[i] = OsdVec3(cagePoints[i].first[0], cagePoints[i].first[1], cagePoints[i].first[2]);
    }

    auto infVertex = triangulation.infinite_vertex();
    std::vector<Triangulation::Cell_handle> cells;
    triangulation.incident_cells(infVertex, std::back_inserter(cells));

    int facetId = 0;
    for (size_t i = 0; i < cells.size(); ++i)
    {
        rawCageIndices.resize((facetId + 1) * 3);

        int infVertexId = cells[i]->index(infVertex);

        quint32 indices[3];
        int currIndex = 0;
        for (int j = 0; j < 4; ++j)
        {
            if (j != infVertexId)
                indices[currIndex++] = cells[i]->vertex(j)->info();
        }

        for (int j = 0; j < 3; ++j)
        {
            rawCageIndices[facetId * 3 + j] = indices[j];
        }

        ++facetId;
    }
    
    Geometry startingCage;
    startingCage.setPositions(rawCagePositions);
    startingCage.setIndices(rawCageIndices);
    startingCage.filterVertices();
    startingCage.generateNormals();
    
    rawCagePositions = startingCage.getPositions();
    rawCageIndices = startingCage.getIndices();
    rawCageNormals = startingCage.getTriangleNormals();
    
    // Fix the cage normals
    for (int i = 0; i < rawCageIndices.size() / 3; ++i)
    {
        // Triangle indices
        int indices[] =
        {
            rawCageIndices[i * 3 + 0],
            rawCageIndices[i * 3 + 1],
            rawCageIndices[i * 3 + 2],
        };

        int vertexCutSlides[] =
        {
            indices[0] / 4,
            indices[1] / 4,
            indices[2] / 4,
        };

        std::sort(vertexCutSlides, vertexCutSlides + 3);

        int enclosedComponentId = -1;

        for (int j = 0; j < componentCutSlides.size(); ++j)
        {
            const auto& slides = componentCutSlides[j];
            if (std::includes(slides.begin(), slides.end(), vertexCutSlides, std::unique(vertexCutSlides, vertexCutSlides + 3)))
            {
                enclosedComponentId = j;
                break;
            }
        }

        if (enclosedComponentId == -1)
        {
            qWarning() << "Unable to identify the corresponding component for cut slides" << vertexCutSlides[0] << vertexCutSlides[1] << vertexCutSlides[2];
            m_cageGeometry = new Geometry({}, {});
            return;
        }
        
        // Triange vertices, normal and center
        OsdVec3 triVertices[] =
        {
            rawCagePositions[indices[0]],
            rawCagePositions[indices[1]],
            rawCagePositions[indices[2]],
        };
        OsdVec3 triNormal = rawCageNormals[i];
        OsdVec3 triCenter = (triVertices[0] + triVertices[1] + triVertices[2]) / 3.0f;

        // Compute the average signed distance to the triangle center from the
        // enclosed component vertices
        float avgDistance = 0.0f;

        const auto& slides = componentCutSlides[enclosedComponentId];
        for (int j = 0; j < slides.size(); ++j)
        {
            for (int k = 0; k < 4; ++k)
            {
                float dist = dot(rawCagePositions[slides[j] * 4 + k] - triCenter, triNormal);
                avgDistance += dist;
            }
        }

        avgDistance = avgDistance / componentIndices[enclosedComponentId].rows();

        // We need to flip if the average distance is positive (triangle is
        // looking toward the vertices, a.k.a. inside)
        if (avgDistance > 0.0f)
        {
            std::swap(rawCageIndices[i * 3 + 1], rawCageIndices[i * 3 + 2]);
            rawCageNormals[i] = rawCageNormals[i] * -1.0f;
        }
    }

    // Extract the triangle-triangle adjacency information
    Eigen::MatrixXd rawCagePositionsIgl(rawCagePositions.size(), 3);
    Eigen::MatrixXi rawCageIndicesIgl(rawCageIndices.size() / 3, 3);

    for (int i = 0; i < rawCagePositions.size(); ++i)
    {
        rawCagePositionsIgl.row(i) = Eigen::RowVector3d(
                    rawCagePositions[i].position[0], rawCagePositions[i].position[1], rawCagePositions[i].position[2]);
    }

    for (int i = 0; i < rawCageIndices.size() / 3; ++i)
    {
        rawCageIndicesIgl.row(i) = Eigen::RowVector3i(
                    rawCageIndices[i * 3 + 0], rawCageIndices[i * 3 + 1], rawCageIndices[i * 3 + 2]);
    }

    TT.clear();
    TTi.clear();

    igl::triangle_triangle_adjacency(rawCageIndicesIgl, TT, TTi);

    // Perform edge flipping on the initial cage
    std::vector<std::array<bool, 3>> edgeFlipProcessed(rawCageIndices.size() / 3);

    for (int i = 0; i < rawCageIndices.size() / 3; ++i)
    {
        // Extract the indices of the closed triangle loop
        quint32 triIndices[] =
        {
            rawCageIndices[i * 3 + 0],
            rawCageIndices[i * 3 + 1],
            rawCageIndices[i * 3 + 2],
            rawCageIndices[i * 3 + 0],
        };

        // Go over each edge
        for (int j = 0; j < 3; ++j)
        {
            qDebug() << i << j << TT[i][j].size();

            // Don't flip edges on the cut slides/cross sections
            if (triIndices[j] / 4 == triIndices[j + 1] / 4)
                continue;

            qDebug() << i << j << TT[i][j].size();

            // Process
            for (int k = 0; k < TT[i][j].size(); ++k)
            {

            }
        }
    }

    // Compute the final normals and compute the incident triangles for the cage vertices
    QVector<OsdVec3> rawCageCenters(rawCageIndices.size() / 3);
    QVector<OsdVec3> rawCageOutermosts(rawCageIndices.size() / 3);
    QVector<OsdVec3> rawCageAnchors(rawCageIndices.size() / 3);
    QVector<OsdVec3> refinedCagePositions = rawCagePositions;
    std::vector<std::vector<int>> incidentTriangles;
    std::vector<std::vector<int>> incidentTriangleIds;

    for (int i = 0; i < rawCageIndices.size() / 3; ++i)
    {
        rawCageIndicesIgl.row(i) = Eigen::RowVector3i(
                    rawCageIndices[i * 3 + 0], rawCageIndices[i * 3 + 1], rawCageIndices[i * 3 + 2]);
    }

    igl::vertex_triangle_adjacency(rawCagePositionsIgl, rawCageIndicesIgl, incidentTriangles, incidentTriangleIds);

    // Iteratively refine the cage
    static const float REFINEMENT_ITERATIONS = 10;
    static const float OUTERMOST_THRESHOLD = 0.5f;

    for (int i = 0; i < REFINEMENT_ITERATIONS; ++i)
    {
        Geometry currentCage;
        currentCage.setPositions(refinedCagePositions);
        currentCage.setIndices(rawCageIndices);
        currentCage.generateNormals();

        rawCagePositions = refinedCagePositions;
        rawCageNormals = currentCage.getTriangleNormals();

        for (int j = 0; j < rawCageIndices.size() / 3; ++j)
        {
            OsdVec3 triVertices[] =
            {
                rawCagePositions[rawCageIndices[j * 3 + 0]],
                rawCagePositions[rawCageIndices[j * 3 + 1]],
                rawCagePositions[rawCageIndices[j * 3 + 2]],
            };
            OsdVec3 triNormal = rawCageNormals[j];
            OsdVec3 triCenter = (triVertices[0] + triVertices[1] + triVertices[2]) / 3.0f;

            OsdVec3 outermost;
            float outermostDist = -FLT_MAX;
            int outermostId = -1;
            for (int k = 0; k < meshVerticesRaw.size(); ++k)
            {
                float dist = dot(meshVerticesRaw[k] - triCenter, triNormal);
                if (dist > outermostDist)
                {
                    outermost = meshVerticesRaw[k];
                    outermostDist = dist;
                    outermostId = k;
                }
            }

            OsdVec3 anchor;
            if (outermostDist < -OUTERMOST_THRESHOLD)
            {
                // Inside the cage
                anchor = triCenter;
            }
            else
            {
                // Outside the cage
                anchor = outermost + triNormal * OUTERMOST_THRESHOLD;
            }

            rawCageCenters[j] = triCenter;
            rawCageOutermosts[j] = outermost;
            rawCageAnchors[j] = anchor;
        }

        for (int j = 0; j < rawCagePositions.size(); ++j)
        {
            // Create the problem object
            ceres::Problem problem;

            // Instantiate the cost function
            RefinementCostFunction* cost = new RefinementCostFunction(
                        rawCagePositions[j], rawCageNormals, rawCageAnchors, incidentTriangles[j]);

            // Create the cost function
            ceres::CostFunction* costFn =
                    new ceres::AutoDiffCostFunction<RefinementCostFunction, 1, 3>(cost);

            // Result of the minimalization
            double result[] =
            {
                rawCagePositions[j].position[0],
                rawCagePositions[j].position[1],
                rawCagePositions[j].position[2],
            };

            // Add the data
            problem.AddResidualBlock(costFn, nullptr, result);

            // Create the solver options
            ceres::Solver::Options options;
            options.max_num_iterations = 1000;
            options.linear_solver_type = ceres::DENSE_QR;
            //options.logging_type = ceres::SILENT;

            // Solve the problem
            ceres::Solver::Summary summary;
            ceres::Solve(options, &problem, &summary);

            refinedCagePositions[j] = OsdVec3(result[0], result[1], result[2]);
        }
    }

    // Vectors holding the generated geometry
    QVector<OsdVec3> cagePositionsGeometry = refinedCagePositions;
    QVector<quint32> cageIndicesGeometry = rawCageIndices;

    // Upload to the GPU
    m_cageGeometry = new Geometry(cagePositionsGeometry, cageIndicesGeometry);
}
