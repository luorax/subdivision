#include "geometry.h"

Qt3DRender::QAttribute* getPositionAttrib(Qt3DRender::QGeometry* geometry)
{
    auto attributes = geometry->attributes();
    for (auto attrib: attributes)
        if (attrib->name() == Qt3DRender::QAttribute::defaultPositionAttributeName())
            return attrib;

    return nullptr;
}

Qt3DRender::QAttribute* getIndexAttrib(Qt3DRender::QGeometry* geometry)
{
    auto attributes = geometry->attributes();
    for (auto attrib: attributes)
        if (attrib->attributeType() == Qt3DRender::QAttribute::IndexAttribute)
            return attrib;

    return nullptr;
}

QByteArray getBufferData(Qt3DRender::QBuffer* buffer)
{
    return buffer->dataGenerator() ? (*buffer->dataGenerator())() : buffer->data();
}

#define BYTE_INDEX_PACKED(ATTRIB, INDEX, STRIDE) (((INDEX) * STRIDE) + ATTRIB->byteOffset())
#define BYTE_INDEX(ATTRIB, INDEX) BYTE_INDEX_PACKED(ATTRIB, INDEX, ATTRIB->byteStride())

Geometry::Geometry():
    m_geometry(nullptr),
    m_position(nullptr),
    m_normal(nullptr),
    m_color(nullptr),
    m_index(nullptr)
{
}

Geometry::Geometry(const QVector<OsdVec3>& positions, const QVector<quint32>& indices, float min, float max):
    m_positions(positions),
    m_indices(indices),
    m_geometry(nullptr),
    m_position(nullptr),
    m_normal(nullptr),
    m_color(nullptr),
    m_index(nullptr)
{
    // Finalize the mesh data and upload it
    rescale(min, max);
    filterVertices();
    generateNormals();
    setColors(QColor(Qt::white));
    upload();
}

Geometry::Geometry(Qt3DRender::QGeometry *geometry, float min, float max):
    m_geometry(nullptr),
    m_position(nullptr),
    m_normal(nullptr),
    m_color(nullptr),
    m_index(nullptr)
{
    // Extract the relevant attributes
    Qt3DRender::QAttribute* position = getPositionAttrib(geometry);
    Qt3DRender::QAttribute* index = getIndexAttrib(geometry);

    // Extract their buffers
    QByteArray positionData = getBufferData(position->buffer());
    QByteArray indexData = getBufferData(index->buffer());

    m_positions.resize(position->count());
    m_indices.resize(index->count());

    // Convert the position data
    for (int i = 0; i < m_positions.size(); ++i)
    {
        OsdVec3* positionPtr = (OsdVec3*) (&positionData.data()[BYTE_INDEX(position, i)]);
        m_positions[i] = positionPtr[0];
    }

    // Convert the index data
    for (int i = 0; i < m_indices.size(); ++i)
    {
        switch (index->vertexBaseType())
        {
            case Qt3DRender::QAttribute::UnsignedByte:
            {
                quint8* indexPtr = (quint8*) (&indexData.data()[BYTE_INDEX_PACKED(index, i, sizeof(quint8))]);
                m_indices[i] = indexPtr[0];
                break;
            }

            case Qt3DRender::QAttribute::UnsignedShort:
            {
                quint16* indexPtr = (quint16*) (&indexData.data()[BYTE_INDEX_PACKED(index, i, sizeof(quint16))]);
                m_indices[i] = indexPtr[0];
                break;
            }

            case Qt3DRender::QAttribute::UnsignedInt:
            {
                quint32* indexPtr = (quint32*) (&indexData.data()[BYTE_INDEX_PACKED(index, i, sizeof(quint32))]);
                m_indices[i] = indexPtr[0];
                break;
            }
        };
    }

    // Finalize the mesh data and upload it
    rescale(min, max);
    filterVertices();
    generateNormals();
    setColors(QColor(Qt::white));
    upload();
}

const QVector<OsdVec3>& Geometry::getPositions() const
{
    return m_positions;
}

const QVector<OsdVec3>& Geometry::getNormals() const
{
    return m_normals;
}

const QVector<OsdVec3>& Geometry::getTriangleNormals() const
{
    return m_triangleNormals;
}

const QVector<QColor>& Geometry::getColors() const
{
    return m_colors;
}

const QVector<quint32>& Geometry::getIndices() const
{
    return m_indices;
}

Qt3DRender::QGeometry* Geometry::getGeometry() const
{
    return m_geometry;
}

Qt3DRender::QAttribute* Geometry::getPosition() const
{
    return m_position;
}

Qt3DRender::QAttribute* Geometry::getNormal() const
{
    return m_normal;
}

Qt3DRender::QAttribute* Geometry::getColor() const
{
    return m_color;
}

Qt3DRender::QAttribute* Geometry::getIndex() const
{
    return m_index;
}

void Geometry::setPositions(const QVector<OsdVec3>& positions)
{
    m_positions = positions;
}

void Geometry::setNormals(const QVector<OsdVec3>& normals)
{
    m_normals = normals;
}

void Geometry::setColors(const QVector<QColor>& colors)
{
    m_colors = colors;
}

void Geometry::setColors(QColor color)
{
    m_colors = QVector<QColor>(m_positions.size(), color);
}

void Geometry::setIndices(const QVector<quint32>& indices)
{
    m_indices = indices;
}

bool qMapLessThanKey(const OsdVec3& lhs, const OsdVec3& rhs)
{
    return lhs.position[0] < rhs.position[0]
        || (lhs.position[0] == rhs.position[0] && (lhs.position[1] < rhs.position[1]
        || (lhs.position[1] == rhs.position[1] && lhs.position[2] < rhs.position[2])));
}

void Geometry::rescale(float minExtent, float maxExtent)
{
    // Compute the extent of the mesh
    OsdVec3 min = OsdVec3(900000, 900000, 900000), max = OsdVec3(-900000, -900000, -900000);
    for (int i = 0; i < m_positions.size(); ++i)
    {
        OsdVec3 vertex = m_positions[i];

        min = vecMin(min, vertex);
        max = vecMax(max, vertex);
    }

    // Get the normalization factor
    auto meshExtent = extent(min, max);
    auto scale = (meshExtent < minExtent ? minExtent / meshExtent : ((meshExtent > maxExtent) ? maxExtent / meshExtent : 1.0f));

    // Scale the vertices
    for (int i = 0; i < m_positions.size(); ++i)
    {
        m_positions[i] = m_positions[i] * scale;
    }
}

void Geometry::filterVertices(float epsilon)
{
    // Containers used to remap identical vertices
    QVector<OsdVec3> uniqueVertices;
    QMap<int, int> mapIndices;

    // Get the unique vertices
    for (int i = 0; i < m_positions.size(); ++i)
    {
        // Skip unused vertices
        if (m_indices.count(i) == 0)
        {
            continue;
        }

        // Look for a similar vertex
        int foundIndex = -1;
        for (int j = 0; j < uniqueVertices.size(); ++j)
        {
            float dist = distance(m_positions[i], uniqueVertices[j]);
            if (dist <= epsilon)
            {
                foundIndex = j;
                break;
            }
        }

        // If no similar one is found, then use it
        if (foundIndex == -1)
        {
            uniqueVertices.push_back(m_positions[i]);
            mapIndices[i] = uniqueVertices.size() - 1;
        }

        // Otherwise store the index of the similar vertex
        else
        {
            mapIndices[i] = foundIndex;
        }
    }

    // Update the indices
    auto newIndices = m_indices;
    for (int i = 0; i < m_indices.size(); ++i)
    {
        newIndices[i] = mapIndices[m_indices[i]];
    }

    // Store the new data
    m_indices = newIndices;
    m_positions = uniqueVertices;
}

void Geometry::generateNormals()
{
    // Resize the normals vectors
    m_normals.resize(m_positions.size());
    m_triangleNormals.resize(m_indices.size() / 3);

    // Generate normals
    for (int i = 0; i < m_indices.size() / 3; ++i)
    {
        quint32 indices[] =
        {
            m_indices[i * 3 + 0],
            m_indices[i * 3 + 1],
            m_indices[i * 3 + 2],
        };

        // Extract the relevant vertices
        OsdVec3 v[] =
        {
            m_positions[indices[0]],
            m_positions[indices[1]],
            m_positions[indices[2]],
        };

        // Take the cross product
        OsdVec3 e1 = OsdVec3(v[1].position[0] - v[0].position[0], v[1].position[1] - v[0].position[1], v[1].position[2] - v[0].position[2]);
        OsdVec3 e2 = OsdVec3(v[2].position[0] - v[0].position[0], v[2].position[1] - v[0].position[1], v[2].position[2] - v[0].position[2]);
        OsdVec3 triangleNormal = normalize(cross(e1, e2));

        // Store the triangle normal
        m_triangleNormals[i] = triangleNormal;

        // Accumulate that normal on all verts that are part of that face
        for (int j = 0; j < 3; ++j)
        {
            m_normals[indices[j]] = m_normals[indices[j]] + triangleNormal;
        }
    }

    // Normalize the normals
    for (int i = 0; i < m_positions.size(); ++i)
    {
        m_normals[i] = normalize(m_normals[i]);
    }
}

void Geometry::upload()
{
    // Release any previous objects
    if (m_geometry != nullptr)
    {
        delete m_position;
        delete m_normal;
        delete m_color;
        delete m_index;
        delete m_geometry;
    }

    // Create the geometry instance
    m_geometry = new Qt3DRender::QGeometry;

    // Create the position attrib
    m_position = new Qt3DRender::QAttribute;

    m_position->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    m_position->setByteOffset(0);
    m_position->setByteStride(4 * sizeof(float));
    m_position->setCount(m_positions.size());
    m_position->setDivisor(0);
    m_position->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
    m_position->setDataType(Qt3DRender::QAttribute::Float);
    m_position->setDataSize(3);

    QByteArray positionsRaw(m_positions.size() * sizeof(float) * 4, '\0');
    for (int i = 0; i < m_positions.size(); ++i)
    {
        OsdVec3* positionPtr = (OsdVec3*) (&positionsRaw.data()[BYTE_INDEX(m_position, i)]);
        positionPtr[0] = m_positions[i];
    }

    Qt3DRender::QBuffer* positionBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer);
    positionBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
    positionBuffer->setData(positionsRaw);

    m_position->setBuffer(positionBuffer);

    m_geometry->addAttribute(m_position);

    // Create the normal attrib
    m_normal = new Qt3DRender::QAttribute;

    m_normal->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    m_normal->setByteOffset(0);
    m_normal->setByteStride(4 * sizeof(float));
    m_normal->setCount(m_normals.size());
    m_normal->setDivisor(0);
    m_normal->setName(Qt3DRender::QAttribute::defaultNormalAttributeName());
    m_normal->setDataType(Qt3DRender::QAttribute::Float);
    m_normal->setDataSize(3);

    QByteArray normalsRaw(m_normals.size() * sizeof(float) * 4, '\0');
    for (int i = 0; i < m_normals.size(); ++i)
    {
        OsdVec3* normalPtr = (OsdVec3*) (&normalsRaw.data()[BYTE_INDEX(m_normal, i)]);
        normalPtr[0] = m_normals[i];
    }

    Qt3DRender::QBuffer* normalBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer);
    normalBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
    normalBuffer->setData(normalsRaw);

    m_normal->setBuffer(normalBuffer);

    m_geometry->addAttribute(m_normal);

    // Create the color attrib
    m_color = new Qt3DRender::QAttribute;

    m_color->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    m_color->setByteOffset(0);
    m_color->setByteStride(4 * sizeof(float));
    m_color->setCount(m_colors.size());
    m_color->setDivisor(0);
    m_color->setName(Qt3DRender::QAttribute::defaultColorAttributeName());
    m_color->setDataType(Qt3DRender::QAttribute::Float);
    m_color->setDataSize(4);

    QByteArray colorsRaw(m_colors.size() * sizeof(float) * 4, '\0');
    for (int i = 0; i < m_colors.size(); ++i)
    {
        float* colorPtr = (float*) (&colorsRaw.data()[BYTE_INDEX(m_color, i)]);
        colorPtr[0] = m_colors[i].redF();
        colorPtr[1] = m_colors[i].greenF();
        colorPtr[2] = m_colors[i].blueF();
        colorPtr[3] = m_colors[i].alphaF();
    }

    Qt3DRender::QBuffer* colorBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer);
    colorBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
    colorBuffer->setData(colorsRaw);

    m_color->setBuffer(colorBuffer);

    m_geometry->addAttribute(m_color);

    // Create the index attrib
    m_index = new Qt3DRender::QAttribute;

    m_index->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    m_index->setByteOffset(0);
    m_index->setByteStride(0);
    m_index->setCount(m_indices.size());
    m_index->setDivisor(0);
    m_index->setName("");
    m_index->setDataType(Qt3DRender::QAttribute::UnsignedInt);
    m_index->setDataSize(1);

    QByteArray indicesRaw(m_indices.size() * sizeof(quint32), '\0');
    for (int i = 0; i < m_indices.size(); ++i)
    {
        quint32* indexPtr = (quint32*) (&indicesRaw.data()[BYTE_INDEX_PACKED(m_index, i, sizeof(quint32))]);
        indexPtr[0] = m_indices[i];
    }

    Qt3DRender::QBuffer* indexBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::IndexBuffer);
    indexBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
    indexBuffer->setData(indicesRaw);

    m_index->setBuffer(indexBuffer);

    m_geometry->addAttribute(m_index);
}
