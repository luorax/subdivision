#include "osdvec3.h"

OsdVec3::OsdVec3():
    position{ 0.0f, 0.0f, 0.0f }
{}

OsdVec3::OsdVec3(float a, float b, float c):
    position{ a, b, c }
{}

OsdVec3::OsdVec3(const OsdVec3& src)
{
    position[0] = src.position[0];
    position[1] = src.position[1];
    position[2] = src.position[2];
}

OsdVec3 OsdVec3::operator+(const OsdVec3& rhs) const
{
    return OsdVec3(
        position[0] + rhs.position[0],
        position[1] + rhs.position[1],
        position[2] + rhs.position[2]);
}

OsdVec3 OsdVec3::operator-(const OsdVec3& rhs) const
{
    return OsdVec3(
        position[0] - rhs.position[0],
        position[1] - rhs.position[1],
        position[2] - rhs.position[2]);
}

OsdVec3 OsdVec3::operator*(const OsdVec3& rhs) const
{
    return OsdVec3(
        position[0] * rhs.position[0],
        position[1] * rhs.position[1],
        position[2] * rhs.position[2]);
}

OsdVec3 OsdVec3::operator/(const OsdVec3& rhs) const
{
    return OsdVec3(
        position[0] / rhs.position[0],
        position[1] / rhs.position[1],
        position[2] / rhs.position[2]);
}

OsdVec3 OsdVec3::operator+(float rhs) const
{
    return OsdVec3(
        position[0] + rhs,
        position[1] + rhs,
        position[2] + rhs);
}

OsdVec3 OsdVec3::operator-(float rhs) const
{
    return OsdVec3(
        position[0] - rhs,
        position[1] - rhs,
        position[2] - rhs);
}

OsdVec3 OsdVec3::operator*(float rhs) const
{
    return OsdVec3(
        position[0] * rhs,
        position[1] * rhs,
        position[2] * rhs);
}

OsdVec3 OsdVec3::operator/(float rhs) const
{
    return OsdVec3(
        position[0] / rhs,
        position[1] / rhs,
        position[2] / rhs);
}

void OsdVec3::Clear()
{
    position[0] = position[1] = position[2] = 0.0f;
}

void OsdVec3::AddWithWeight(const OsdVec3& src, float weight)
{
    position[0] += weight * src.position[0];
    position[1] += weight * src.position[1];
    position[2] += weight * src.position[2];
}

void OsdVec3::SetPosition(float x, float y, float z)
{
    position[0] = x;
    position[1] = y;
    position[2] = z;
}

const float* OsdVec3::GetPosition() const
{
    return position;
}

float length(OsdVec3 v)
{
    return qSqrt(dot(v, v));
}

OsdVec3 normalize(OsdVec3 v)
{
    float rn = 1.0f / sqrtf(v.position[0] * v.position[0] + v.position[1] * v.position[1] + v.position[2] * v.position[2]);

    v.position[0] *= rn;
    v.position[1] *= rn;
    v.position[2] *= rn;

    return v;
}

OsdVec3 cross(OsdVec3 v1, OsdVec3 v2)
{
    OsdVec3 vOut;

    vOut.position[0] = v1.position[1] * v2.position[2] - v1.position[2] * v2.position[1];
    vOut.position[1] = v1.position[2] * v2.position[0] - v1.position[0] * v2.position[2];
    vOut.position[2] = v1.position[0] * v2.position[1] - v1.position[1] * v2.position[0];

    return vOut;
}

float dot(OsdVec3 v1, OsdVec3 v2)
{
    return v1.position[0] * v2.position[0] +
            v1.position[1] * v2.position[1] +
            v1.position[2] * v2.position[2];
}

float distance(OsdVec3 v1, OsdVec3 v2)
{
    return length(v1 - v2);
}

OsdVec3 vecMin(OsdVec3 v1, OsdVec3 v2)
{
    v1.position[0] = v1.position[0] < v2.position[0] ? v1.position[0] : v2.position[0];
    v1.position[1] = v1.position[1] < v2.position[1] ? v1.position[1] : v2.position[1];
    v1.position[2] = v1.position[2] < v2.position[2] ? v1.position[2] : v2.position[2];

    return v1;
}

OsdVec3 vecMax(OsdVec3 v1, OsdVec3 v2)
{
    v1.position[0] = v1.position[0] < v2.position[0] ? v2.position[0] : v1.position[0];
    v1.position[1] = v1.position[1] < v2.position[1] ? v2.position[1] : v1.position[1];
    v1.position[2] = v1.position[2] < v2.position[2] ? v2.position[2] : v1.position[2];

    return v1;
}

float compMin(OsdVec3 v)
{
    v.position[0] = v.position[0] < v.position[1] ? v.position[0] : v.position[1];
    return v.position[0] < v.position[2] ? v.position[0] : v.position[2];
}

float compMax(OsdVec3 v)
{
    v.position[0] = v.position[0] < v.position[1] ? v.position[1] : v.position[0];
    return v.position[0] < v.position[2] ? v.position[2] : v.position[0];
}

float extent(OsdVec3 v1, OsdVec3 v2)
{
    v1.position[0] = v1.position[0] - v2.position[0];
    v1.position[1] = v1.position[1] - v2.position[1];
    v1.position[2] = v1.position[2] - v2.position[2];

    return sqrtf(v1.position[0] * v1.position[0] +
        v1.position[1] * v1.position[1] +
        v1.position[2] * v1.position[2]);
}

float randFloat(float min, float max)
{
    return min + (max - min) * ((float) qrand() / (float) RAND_MAX);
}
