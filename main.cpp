#include "cage.h"
#include "mesh.h"
#include "scene.h"

int main(int argc, char *argv[])
{
    // The main app
    QApplication app(argc, argv);

    app.setStyle("fusion");

    // Create the 3D window
    Qt3DExtras::Qt3DWindow* view = new Qt3DExtras::Qt3DWindow;
    view->setTitle(QStringLiteral("OpenSubdivision"));

    // Create the subdivision mesh
    Mesh* mesh = new Mesh;

    // Create the cage
    Cage* cage = new Cage;

    // Create the deformation
    Deformation* deformation = new Deformation;

    // Create the main scene
    Scene* scene = new Scene(view, mesh, cage, deformation);
    scene->loadSphere();

    // Create the menu bar
    QMenuBar* menuBar = new QMenuBar;

    // File menu
    QMenu* fileMenu = new QMenu;
    menuBar->addMenu(fileMenu);
    fileMenu->setTitle("File");

    // File->quit
    QAction* quitAction = new QAction;
    fileMenu->addAction(quitAction);
    quitAction->setText("Quit");
    QObject::connect(quitAction, &QAction::triggered, scene, &Scene::quit);

    // Edit menu
    QMenu* editMenu = new QMenu;
    menuBar->addMenu(editMenu);
    editMenu->setTitle("Edit");

    // Edit->Undo
    QAction* undoAction = new QAction;
    editMenu->addAction(undoAction);
    undoAction->setText("Undo");
    QObject::connect(undoAction, &QAction::triggered, scene, &Scene::undoSubdivision);

    // Edit->Redo
    QAction* redoAction = new QAction;
    editMenu->addAction(redoAction);
    redoAction->setText("Redo");
    QObject::connect(redoAction, &QAction::triggered, scene, &Scene::redoSubdivision);

    // View menu
    QMenu* viewMenu = new QMenu;
    menuBar->addMenu(viewMenu);
    viewMenu->setTitle("View");

    viewMenu->addSection("Original Mesh");

    // Original Mesh Display modes action group
    QActionGroup* originalMeshDisplayModeGroup = new QActionGroup(viewMenu);
    QObject::connect(originalMeshDisplayModeGroup, &QActionGroup::triggered, scene, &Scene::changeOriginalMeshDisplayMode);

    // View->Original Mesh Display Mode->Gouraud
    QAction* originalMeshGouraudAction = new QAction;
    viewMenu->addAction(originalMeshGouraudAction);
    originalMeshDisplayModeGroup->addAction(originalMeshGouraudAction);
    originalMeshGouraudAction->setText("Gouraud");
    originalMeshGouraudAction->setCheckable(true);

    // View->Original Mesh Display Mode->Phong
    QAction* originalMeshPhongAction = new QAction;
    viewMenu->addAction(originalMeshPhongAction);
    originalMeshDisplayModeGroup->addAction(originalMeshPhongAction);
    originalMeshPhongAction->setText("Phong");
    originalMeshPhongAction->setCheckable(true);

    // View->Original Mesh Display Mode->Unlit
    QAction* originalMeshUnlitAction = new QAction;
    viewMenu->addAction(originalMeshUnlitAction);
    originalMeshDisplayModeGroup->addAction(originalMeshUnlitAction);
    originalMeshUnlitAction->setText("Unlit");
    originalMeshUnlitAction->setCheckable(true);

    // View->Original Mesh Display Mode->Wireframe
    QAction* originalMeshWireframeAction = new QAction;
    viewMenu->addAction(originalMeshWireframeAction);
    originalMeshDisplayModeGroup->addAction(originalMeshWireframeAction);
    originalMeshWireframeAction->setText("Wireframe");
    originalMeshWireframeAction->setCheckable(true);

    // View->Original Mesh Display Mode->Invisible
    QAction* originalMeshInvisibleAction = new QAction;
    viewMenu->addAction(originalMeshInvisibleAction);
    originalMeshDisplayModeGroup->addAction(originalMeshInvisibleAction);
    originalMeshInvisibleAction->setText("Invisible");
    originalMeshInvisibleAction->setCheckable(true);

    // Set the default mesh display mode
    originalMeshInvisibleAction->setChecked(true);

    viewMenu->addSection("Deformed Mesh");

    // Original Mesh Display modes action group
    QActionGroup* deformedMeshDisplayModeGroup = new QActionGroup(viewMenu);
    QObject::connect(deformedMeshDisplayModeGroup, &QActionGroup::triggered, scene, &Scene::changeDeformedMeshDisplayMode);

    // View->Original Mesh Display Mode->Gouraud
    QAction* deformedMeshGouraudAction = new QAction;
    viewMenu->addAction(deformedMeshGouraudAction);
    deformedMeshDisplayModeGroup->addAction(deformedMeshGouraudAction);
    deformedMeshGouraudAction->setText("Gouraud");
    deformedMeshGouraudAction->setCheckable(true);

    // View->Original Mesh Display Mode->Phong
    QAction* deformedMeshPhongAction = new QAction;
    viewMenu->addAction(deformedMeshPhongAction);
    deformedMeshDisplayModeGroup->addAction(deformedMeshPhongAction);
    deformedMeshPhongAction->setText("Phong");
    deformedMeshPhongAction->setCheckable(true);

    // View->Original Mesh Display Mode->Unlit
    QAction* deformedMeshUnlitAction = new QAction;
    viewMenu->addAction(deformedMeshUnlitAction);
    deformedMeshDisplayModeGroup->addAction(deformedMeshUnlitAction);
    deformedMeshUnlitAction->setText("Unlit");
    deformedMeshUnlitAction->setCheckable(true);

    // View->Original Mesh Display Mode->Wireframe
    QAction* deformedMeshWireframeAction = new QAction;
    viewMenu->addAction(deformedMeshWireframeAction);
    deformedMeshDisplayModeGroup->addAction(deformedMeshWireframeAction);
    deformedMeshWireframeAction->setText("Wireframe");
    deformedMeshWireframeAction->setCheckable(true);

    // View->Original Mesh Display Mode->Invisible
    QAction* deformedMeshInvisibleAction = new QAction;
    viewMenu->addAction(deformedMeshInvisibleAction);
    deformedMeshDisplayModeGroup->addAction(deformedMeshInvisibleAction);
    deformedMeshInvisibleAction->setText("Invisible");
    deformedMeshInvisibleAction->setCheckable(true);

    // Set the default mesh display mode
    deformedMeshPhongAction->setChecked(true);

    viewMenu->addSection("Cut Slide");

    // Cut slide display modes action group
    QActionGroup* cutSlideDisplayModeGroup = new QActionGroup(viewMenu);
    QObject::connect(cutSlideDisplayModeGroup, &QActionGroup::triggered, scene, &Scene::changeCutSlideDisplayMode);

    // View->Cut Slide Display Mode->Gouraud
    QAction* cutSlideGouraudAction = new QAction;
    viewMenu->addAction(cutSlideGouraudAction);
    cutSlideDisplayModeGroup->addAction(cutSlideGouraudAction);
    cutSlideGouraudAction->setText("Gouraud");
    cutSlideGouraudAction->setCheckable(true);

    // View->Cut Slide Display Mode->Phong
    QAction* cutSlidePhongAction = new QAction;
    viewMenu->addAction(cutSlidePhongAction);
    cutSlideDisplayModeGroup->addAction(cutSlidePhongAction);
    cutSlidePhongAction->setText("Phong");
    cutSlidePhongAction->setCheckable(true);

    // View->Cut Slide Display Mode->Unlit
    QAction* cutSlideUnlitAction = new QAction;
    viewMenu->addAction(cutSlideUnlitAction);
    cutSlideDisplayModeGroup->addAction(cutSlideUnlitAction);
    cutSlideUnlitAction->setText("Unlit");
    cutSlideUnlitAction->setCheckable(true);

    // View->Cut Slide  Display Mode->Wireframe
    QAction* cutSlideWireframeAction = new QAction;
    viewMenu->addAction(cutSlideWireframeAction);
    cutSlideDisplayModeGroup->addAction(cutSlideWireframeAction);
    cutSlideWireframeAction->setText("Wireframe");
    cutSlideWireframeAction->setCheckable(true);

    // View->Cut Slide  Display Mode->Invisible
    QAction* cutSlideInvisibleAction = new QAction;
    viewMenu->addAction(cutSlideInvisibleAction);
    cutSlideDisplayModeGroup->addAction(cutSlideInvisibleAction);
    cutSlideInvisibleAction->setText("Invisible");
    cutSlideInvisibleAction->setCheckable(true);

    // Set the default cut slide display mode
    cutSlideWireframeAction->setChecked(true);

    viewMenu->addSection("Cage");

    // Cage display modes action group
    QActionGroup* cageDisplayModeGroup = new QActionGroup(viewMenu);
    QObject::connect(cageDisplayModeGroup, &QActionGroup::triggered, scene, &Scene::changeCageDisplayMode);

    // View->Cage Display Mode->Gouraud
    QAction* cageGouraudAction = new QAction;
    viewMenu->addAction(cageGouraudAction);
    cageDisplayModeGroup->addAction(cageGouraudAction);
    cageGouraudAction->setText("Gouraud");
    cageGouraudAction->setCheckable(true);

    // View->Cage Display Mode->Phong
    QAction* cagePhongAction = new QAction;
    viewMenu->addAction(cagePhongAction);
    cageDisplayModeGroup->addAction(cagePhongAction);
    cagePhongAction->setText("Phong");
    cagePhongAction->setCheckable(true);

    // View->Cage Display Mode->Unlit
    QAction* cageUnlitAction = new QAction;
    viewMenu->addAction(cageUnlitAction);
    cageDisplayModeGroup->addAction(cageUnlitAction);
    cageUnlitAction->setText("Unlit");
    cageUnlitAction->setCheckable(true);

    // View->Cage Display Mode->Wireframe
    QAction* cageWireframeAction = new QAction;
    viewMenu->addAction(cageWireframeAction);
    cageDisplayModeGroup->addAction(cageWireframeAction);
    cageWireframeAction->setText("Wireframe");
    cageWireframeAction->setCheckable(true);

    // View->Cage Display Mode->Invisible
    QAction* cageInvisibleAction = new QAction;
    viewMenu->addAction(cageInvisibleAction);
    cageDisplayModeGroup->addAction(cageInvisibleAction);
    cageInvisibleAction->setText("Invisible");
    cageInvisibleAction->setCheckable(true);

    // Set the default cage display mode
    cageWireframeAction->setChecked(true);

    // Mesh menu
    QMenu* meshMenu = new QMenu;
    menuBar->addMenu(meshMenu);
    meshMenu->setTitle("Mesh");

    // Mesh->cube
    QAction* loadCubeAction = new QAction;
    meshMenu->addAction(loadCubeAction);
    loadCubeAction->setText("Cube");
    QObject::connect(loadCubeAction, &QAction::triggered, scene, &Scene::loadCube);

    // Mesh->tetrahedron
    QAction* loadTetrahedronAction = new QAction;
    meshMenu->addAction(loadTetrahedronAction);
    loadTetrahedronAction->setText("Tetrahedron (Dual)");
    QObject::connect(loadTetrahedronAction, &QAction::triggered, scene, &Scene::loadTetrahedronDual);

    // Mesh->octahedron
    QAction* loadOctahedronAction = new QAction;
    meshMenu->addAction(loadOctahedronAction);
    loadOctahedronAction->setText("Octahedron (Dual)");
    QObject::connect(loadOctahedronAction, &QAction::triggered, scene, &Scene::loadOctahedronDual);

    // Mesh->sphere
    QAction* loadSphereAction = new QAction;
    meshMenu->addAction(loadSphereAction);
    loadSphereAction->setText("Sphere");
    QObject::connect(loadSphereAction, &QAction::triggered, scene, &Scene::loadSphere);

    // Mesh->torus
    QAction* loadTorusAction = new QAction;
    meshMenu->addAction(loadTorusAction);
    loadTorusAction->setText("Torus");
    QObject::connect(loadTorusAction, &QAction::triggered, scene, &Scene::loadTorus);

    // Mesh->load mesh
    QAction* loadMeshAction = new QAction;
    meshMenu->addAction(loadMeshAction);
    loadMeshAction->setText("Load Custom");
    QObject::connect(loadMeshAction, &QAction::triggered, scene, &Scene::loadMesh);

    // Cage menu
    QMenu* cageMenu = new QMenu;
    menuBar->addMenu(cageMenu);
    cageMenu->setTitle("Cage");

    // Cage->Clear cut slides
    QAction* clearCutSlidesAction = new QAction;
    cageMenu->addAction(clearCutSlidesAction);
    clearCutSlidesAction->setText("Clear Cut Slides");
    QObject::connect(clearCutSlidesAction, &QAction::triggered, scene, &Scene::clearCutSlides);

    // Subdivision menu
    QMenu* subdivisionMenu = new QMenu;
    menuBar->addMenu(subdivisionMenu);
    subdivisionMenu->setTitle("Subdivision");

    // Subdivision->Bilinear
    QAction* bilinearAction = new QAction;
    subdivisionMenu->addAction(bilinearAction);
    bilinearAction->setText("Bilinear");
    QObject::connect(bilinearAction, &QAction::triggered, scene, &Scene::bilinearSubdivision);

    // Subdivision->Loop
    QAction* loopAction = new QAction;
    subdivisionMenu->addAction(loopAction);
    loopAction->setText("Loop");
    QObject::connect(loopAction, &QAction::triggered, scene, &Scene::loopSubdivision);

    // Subdivision->Catmull-Clark
    QAction* catmullClarkAction = new QAction;
    subdivisionMenu->addAction(catmullClarkAction);
    catmullClarkAction->setText("Catmull-Clark");
    QObject::connect(catmullClarkAction, &QAction::triggered, scene, &Scene::catmullClarkSubdivision);

    // Noise menu
    QMenu* noiseMenu = new QMenu;
    menuBar->addMenu(noiseMenu);
    noiseMenu->setTitle("Noise");

    // Noise->small noise
    QAction* smallNoiseAction = new QAction;
    noiseMenu->addAction(smallNoiseAction);
    smallNoiseAction->setText("Small Noise");
    QObject::connect(smallNoiseAction, &QAction::triggered, scene, &Scene::smallNoise);

    // Noise->medium noise
    QAction* mediumNoiseAction = new QAction;
    noiseMenu->addAction(mediumNoiseAction);
    mediumNoiseAction->setText("Medium Noise");
    QObject::connect(mediumNoiseAction, &QAction::triggered, scene, &Scene::mediumNoise);

    // Noise->high noise
    QAction* highNoiseAction = new QAction;
    noiseMenu->addAction(highNoiseAction);
    highNoiseAction->setText("High Noise");
    QObject::connect(highNoiseAction, &QAction::triggered, scene, &Scene::highNoise);

    // Create a container for the window
    QWidget* container = QWidget::createWindowContainer(view);
    container->setMinimumSize(QSize(200, 100));
    container->setMaximumSize(view->screen()->size());

    // Create the main layout
    QWidget* widget = new QWidget;
    QHBoxLayout* hLayout = new QHBoxLayout(widget);
    hLayout->setMenuBar(menuBar);
    hLayout->addWidget(container, 1);

    // Show the window
    widget->show();
    widget->resize(800, 600);

    return app.exec();
}
