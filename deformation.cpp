#include "deformation.h"

Deformation::Deformation():
    m_cage(nullptr),
    m_source(nullptr),
    m_target(nullptr)
{}

Deformation::Deformation(Cage* cage, Geometry* source, Geometry* target):
    m_cage(cage),
    m_source(source),
    m_target(target)
{}

float sign(float f)
{
    return f > 0.0f ? 1.0f : f < 0.0f ? -1.0f : 0.0f;
}

static const float PI = qDegreesToRadians(180.0f);
static const float TWO_PI = qDegreesToRadians(360.0f);

float TriIntMine(OsdVec3 p, OsdVec3 v1, OsdVec3 v2, OsdVec3 n)
{
    float temp = dot(v2 - v1, p - v1) / (length(v2 - v1) * length(p - v1));

    if (qAbs(temp) > 1.0f - 0.00000001f)
        return 0.0f;

    float alpha = qAcos(temp);

    if (qAbs(alpha - TWO_PI) < 0.00000001f || qAbs(alpha) < 0.00000001f)
        return 0.0f;

    temp = dot(p - v1, p - v2) / (length(p - v1) * length(p - v2));

    if (qAbs(temp) > 1.0f - 0.00000001f)
        return 0.0f;

    float beta = qAcos(temp);
    float lambda = dot(p - v1, p - v1) * qSin(alpha) * qSin(alpha);
    float c = dot(p - n, p - n);
    float csqr = qSqrt(c);

    float Itheta[2];
    for (int i = 0; i < 2; ++i)
    {
        float theta = PI - alpha - i * beta;
        float S = qSin(theta);
        float C = qCos(theta);

        Itheta[i] = (-sign(S) / 2.0f) *
                (2.0f * csqr * qAtan((csqr * C) / qSqrt(lambda + S * S * c)) +
                qSqrt(lambda) * qLn(
                     (2.0f * qSqrt(lambda) * S * S) / ((1.0f - C) * (1.0f - C)) *
                     (1.0f - (
                          (2.0f * c * C) / (c * (1.0f + C) + lambda + qSqrt(lambda * lambda + lambda * c * S * S)))
                      )
                     )
                 );
    }

    return (-1.0f / (4 * PI)) * qAbs(Itheta[0] - Itheta[1] - csqr * beta);
}

double TriIntArticle(OsdVec3 p, OsdVec3 t1, OsdVec3 t2, OsdVec3 eta)
{

    OsdVec3 t2mt1 = t2-t1;
    OsdVec3 pmt1 = p-t1;
    OsdVec3 pmt2 = p-t2;

    double Spmt1 = length(pmt1);
    double  Spmt2 = length(pmt2);

    double  tempval =   ( dot(t2mt1,pmt1))/ ((length(t2mt1)*length(pmt1)) );
    if (qAbs(tempval)>(1.0-0.00000000001))
        return 0.0;
    double  alpha = qAcos(tempval );

    if ( qAbs(alpha - 3.14159265358979) < 0.00000000001 || qAbs(alpha) < 0.00000000001 )
        return 0.0;


    tempval =   (  dot(pmt1, pmt2))/(  (Spmt1*Spmt2));
    if (qAbs(tempval)>(1.0 - 0.00000000001))
        return 0.0;

    double  beta    = qAcos( tempval );

    double  Lam = dot(pmt1, pmt1) * qSin(alpha) * qSin(alpha);

    double  delta = 3.14159265358979 - alpha;
    OsdVec3 pmeta = p-eta;
    double  c = dot(pmeta, pmeta);
    double  sqrtc = qSqrt(c);
    // direct calculation
    double 	a=Lam;
    double  sqrta = qSqrt(a);
    double  sx = qSin(delta);
    double  cx = qCos(delta);

    double  I=0.0;
    I=-0.5*sign(sx)* ( 2*sqrtc*atan((sqrtc*cx) / (qSqrt(a+c*sx*sx) ) )+sqrta*log(((sqrta*(1-2*c*cx/(c*(1+cx)+a+sqrta*qSqrt(a+c*sx*sx)))))*(2*sx*sx/pow((1-cx),2))));

    sx = sin((delta-beta));
    cx = cos((delta-beta));

    double  II=0.0;
    II=-0.5*sign(sx)* ( 2*sqrtc*qAtan((sqrtc*cx) / (qSqrt(a+c*sx*sx) ) )+sqrta*(log((sqrta*(1-2*c*cx/(c*(1+cx)+a+sqrta*qSqrt(a+c*sx*sx))))*(2*sx*sx/pow((1-cx),2)))));

    double  myInt = (-1.0/(4*3.14159265358979) )*qAbs( I - II -sqrtc*beta);
    return myInt;

}

float TriInt(OsdVec3 p, OsdVec3 v1, OsdVec3 v2, OsdVec3 n)
{
    return TriIntArticle(p, v1, v2, n);
}

void Deformation::computeCoordinates()
{
    // Extract the relevant data
    const auto& meshPositions = m_source->getPositions();
    const auto& cagePositions = m_cage->getCageGeometry()->getPositions();
    const auto& cageNormals = m_cage->getCageGeometry()->getTriangleNormals();
    const auto& cageIndices = m_cage->getCageGeometry()->getIndices();

    // Initialize it to zeros
    m_weights.resize(meshPositions.size());
    for (int i = 0; i < meshPositions.size(); ++i)
    {
        m_weights[i].m_positions.resize(cagePositions.size());
        m_weights[i].m_normals.resize(cageNormals.size());

        for (int j = 0; j < cagePositions.size(); ++j)
        {
            m_weights[i].m_positions[j] = 0.0f;
        }

        for (int j = 0; j < cageNormals.size(); ++j)
        {
            m_weights[i].m_normals[j] = 0.0f;
        }
    }

    // Compute the weights
    for (int i = 0; i < meshPositions.size(); ++i)
    {
        for (int j = 0; j < cageIndices.size() / 3; ++j)
        {
            int baseIndex = j * 3;

            OsdVec3 relativePositions[] =
            {
                cagePositions[cageIndices[baseIndex + 0]] - meshPositions[i],
                cagePositions[cageIndices[baseIndex + 1]] - meshPositions[i],
                cagePositions[cageIndices[baseIndex + 2]] - meshPositions[i],
            };

            OsdVec3 scaledNormal = cageNormals[j] * dot(relativePositions[0], cageNormals[j]);

            float sl[3];
            float Il[3];
            float IIl[3];
            OsdVec3 Nl[3];

            for (int l = 0; l < 3; ++l)
            {
                sl[l] = sign(dot(cross(relativePositions[l % 3] - scaledNormal, relativePositions[(l + 1) % 3] - scaledNormal), cageNormals[j]));
                Il[l] = TriInt(scaledNormal, relativePositions[l % 3], relativePositions[(l + 1) % 3], OsdVec3());
                IIl[l] = TriInt(OsdVec3(), relativePositions[(l + 1) % 3], relativePositions[l % 3], OsdVec3());
                Nl[l] = cross(relativePositions[(l + 1) % 3], relativePositions[l % 3]);
                if (length(Nl[l]) < 0.000001f)
                {
                    Il[l] = 0.0f;
                }
                else
                {
                    Nl[l] = normalize(Nl[l]);
                }
            }

            float I = -qAbs(sl[0] * Il[0] + sl[1] * Il[1] + sl[2] * Il[2]);

            // Store the normal weights
            m_weights[i].m_normals[j] = -I;

            OsdVec3 w = cageNormals[j] * I + Nl[0] * IIl[0] + Nl[1] * IIl[1] + Nl[2] * IIl[2];

            if (length(w) > 0.0000001f)
            {
                for (int l = 0; l < 3; ++l)
                {
                    m_weights[i].m_positions[cageIndices[baseIndex + l]] += dot(Nl[(l + 1) % 3], w) / dot(Nl[(l + 1) % 3], relativePositions[l]);
                }
            }
        }
    }
}

void Deformation::updateCoordinates(const QVector<OsdVec3> &oldCage, const QVector<OsdVec3> &newCage)
{}

void Deformation::deformGeometry()
{
    // Extract the relevant data
    const auto& cagePositions = m_cage->getCageGeometry()->getPositions();
    const auto& cageNormals = m_cage->getCageGeometry()->getTriangleNormals();
    QVector<OsdVec3> deformedPositions(m_source->getPositions().size());

    for (int i = 0; i < deformedPositions.size(); ++i)
    {
        OsdVec3 result;

        for (int j = 0; j < cagePositions.size(); ++j)
        {
            result = result + cagePositions[j] * m_weights[i].m_positions[j];
        }

        for (int j = 0; j < cageNormals.size(); ++j)
        {
            result = result + cageNormals[j] * m_weights[i].m_normals[j];
        }

        deformedPositions[i] = result;
    }

    // Upload the new geometry
    m_target->setPositions(deformedPositions);
    m_target->setColors(m_source->getColors());
    m_target->setIndices(m_source->getIndices());
    m_target->generateNormals();
    m_target->upload();
}

void Deformation::setSource(Geometry* geom)
{
    m_source = geom;
}

void Deformation::setTarget(Geometry* geom)
{
    m_target = geom;
}

void Deformation::setCage(Cage* cage)
{
    m_cage = cage;
}

Geometry* Deformation::getSource() const
{
    return m_source;
}

Geometry* Deformation::getTarget() const
{
    return m_target;
}

Cage* Deformation::getCage() const
{
    return m_cage;
}
