#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "dependencies.h"
#include "osdvec3.h"

class Geometry
{
public:
    Geometry();
    Geometry(const QVector<OsdVec3>& positions, const QVector<quint32>& indices, float min = 16.0f, float max = 64.0f);
    Geometry(Qt3DRender::QGeometry* renderer, float min = 16.0f, float max = 64.0f);

    void rescale(float min = 16.0f, float max = 64.0f);
    void filterVertices(float epsilon = 0.0001f);
    void generateNormals();
    void upload();

    const QVector<OsdVec3>& getPositions() const;
    const QVector<OsdVec3>& getNormals() const;
    const QVector<OsdVec3>& getTriangleNormals() const;
    const QVector<QColor>& getColors() const;
    const QVector<quint32>& getIndices() const;
    Qt3DRender::QGeometry* getGeometry() const;
    Qt3DRender::QAttribute* getPosition() const;
    Qt3DRender::QAttribute* getNormal() const;
    Qt3DRender::QAttribute* getColor() const;
    Qt3DRender::QAttribute* getIndex() const;

    void setPositions(const QVector<OsdVec3>& positions);
    void setNormals(const QVector<OsdVec3>& normals);
    void setColors(QColor color);
    void setColors(const QVector<QColor>& colors);
    void setIndices(const QVector<quint32>& indices);

private:
    QVector<OsdVec3> m_positions;
    QVector<OsdVec3> m_normals;
    QVector<OsdVec3> m_triangleNormals;
    QVector<QColor> m_colors;
    QVector<quint32> m_indices;

    Qt3DRender::QGeometry* m_geometry;
    Qt3DRender::QAttribute* m_position;
    Qt3DRender::QAttribute* m_color;
    Qt3DRender::QAttribute* m_normal;
    Qt3DRender::QAttribute* m_index;
};

#endif // GEOMETRY_H
