#version 330

uniform mat4 mvp;
uniform mat4 modelMatrix;
uniform mat3 modelNormalMatrix;

in vec3 vertexPosition;
in vec3 vertexNormal;
in vec4 vertexColor;

out vec3 position;
out vec3 normal;
out vec4 vertColor;

void main(void)
{
    normal = normalize(modelNormalMatrix * vertexNormal);
    position = vec3(modelMatrix * vec4(vertexPosition, 1));
    vertColor = vertexColor;
    gl_Position = mvp * vec4(vertexPosition, 1);
}
