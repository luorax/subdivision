#include "mesh.h"

#include "opensubdiv/far/topologyDescriptor.h"
#include "opensubdiv/far/primvarRefiner.h"

Mesh::Mesh():
    m_currentLevel(-1)
{}

Mesh::~Mesh()
{
    clearLevels(true);
}

OpenSubdiv::Sdc::SchemeType getSchemeType(Mesh::SubdivisionTechnique technique)
{
    switch (technique)
    {
    case Mesh::SubdivisionTechnique::BILINEAR:
        return OpenSubdiv::Sdc::SCHEME_BILINEAR;

    case Mesh::SubdivisionTechnique::LOOP:
        return OpenSubdiv::Sdc::SCHEME_LOOP;

    case Mesh::SubdivisionTechnique::CATMARK:
        return OpenSubdiv::Sdc::SCHEME_CATMARK;
    };

    return OpenSubdiv::Sdc::SCHEME_CATMARK;
}

void Mesh::subdivide(SubdivisionTechnique technique)
{
    // Clear the undo history first
    clearLevels(false);

    // Extract the appropriate OSD scheme type
    OpenSubdiv::Sdc::SchemeType type = getSchemeType(technique);

    // Create the options structure
    OpenSubdiv::Sdc::Options options;
    options.SetVtxBoundaryInterpolation(OpenSubdiv::Sdc::Options::VTX_BOUNDARY_EDGE_AND_CORNER);
    options.SetFVarLinearInterpolation(OpenSubdiv::Sdc::Options::FVAR_LINEAR_NONE);

    // Extract the current geometry
    auto geometry = m_levels[getCurrentLevel()];

    // Type of topology descriptor
    typedef OpenSubdiv::Far::TopologyDescriptor Descriptor;

    // Allocate and intialize the primvar data for the original coarse vertices
    auto indexCount = geometry->getIndices().size();
    QVector<int> coarseIndexCountBuffer(indexCount / 3);
    QVector<int> coarseIndexBuffer(indexCount);

    for (int i = 0; i < coarseIndexCountBuffer.size(); ++i)
    {
        coarseIndexCountBuffer[i] = 3;
    }

    for (int i = 0; i < coarseIndexBuffer.size(); ++i)
    {
        coarseIndexBuffer[i] = geometry->getIndices()[i];
    }

    // Fill the vertex descriptor
    Descriptor desc;
    desc.numVertices = geometry->getPositions().size();
    desc.numFaces = indexCount / 3;
    desc.numVertsPerFace = coarseIndexCountBuffer.data();
    desc.vertIndicesPerFace = coarseIndexBuffer.data();

    // Instantiate a topology refiner from the descriptor
    OpenSubdiv::Far::TopologyRefiner* refiner =
        OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Create(desc,
            OpenSubdiv::Far::TopologyRefinerFactory<Descriptor>::Options(type, options));

    // Fill the refinement options
    OpenSubdiv::Far::TopologyRefiner::UniformOptions refineOptions(1);
    refineOptions.fullTopologyInLastLevel = true;

    // Uniformly refine the topolgy once
    refiner->RefineUniform(refineOptions);

    // Extract the last refiner level
    const OpenSubdiv::Far::TopologyLevel& refLastLevel = refiner->GetLevel(1);

    // Determine the sizes for our needs:
    int nCoarseVerts = geometry->getPositions().size();
    int nCoarseFaces = indexCount / 3;
    int nFineVerts   = refLastLevel.GetNumVertices();
    int nFineFaces   = refLastLevel.GetNumFaces();
    int nTotalVerts  = nCoarseVerts + nFineVerts;
    int nTotalFaces  = nCoarseFaces + nFineFaces;

    // Allocate and intialize the various data for the original vertices
    QVector<OsdVec3> coarsePosBuffer = geometry->getPositions();

    // Allocate the final storage
    QVector<OsdVec3> finePosBuffer(nFineVerts);

    // Create the refiner object
    OpenSubdiv::Far::PrimvarRefiner primvarRefiner(*refiner);

    // Interpolate the data
    primvarRefiner.Interpolate(1, coarsePosBuffer, finePosBuffer);

    // Create temporary buffers to hold the GPU data
    int triangleCount = refLastLevel.GetFaceVertices(0).size() == 3 ? 1 : 2;
    QVector<quint32> fineIndices(nFineFaces * triangleCount * 3);

    // Generate the new indices
    for (int i = 0; i < nFineFaces; ++i)
    {
        OpenSubdiv::Far::ConstIndexArray indices = refLastLevel.GetFaceVertices(i);
        int baseIndex = i * triangleCount * 3;

        for (int j = 0; j < indices.size() - 2; ++j)
        {
            fineIndices[baseIndex + j * 3] = indices[0];
            fineIndices[baseIndex + j * 3 + 1] = indices[j + 1];
            fineIndices[baseIndex + j * 3 + 2] = indices[j + 2];
        }
    }

    // Change to the new geometry
    m_levels.push_back(new Geometry(finePosBuffer, fineIndices));
    m_currentLevel = m_levels.size() - 1;
}

void Mesh::noise(int power, float divisor)
{
    // Clear the undo history first
    clearLevels(false);

    // Extract the current geometry
    auto geometry = m_levels[getCurrentLevel()];

    // Extract the vertices
    auto positions = geometry->getPositions();
    auto normals = geometry->getNormals();

    // Compute the extent of the mesh
    OsdVec3 min = OsdVec3(900000, 900000, 900000), max = OsdVec3(-900000, -900000, -900000);
    for (int i = 0; i < positions.size(); ++i)
    {
        min = vecMin(min, positions[i]);
        max = vecMax(max, positions[i]);
    }

    // Rescale the scale
    //auto meshExtent = extent(min, max);
    auto meshExtent = compMin(OsdVec3(max.position[0] - min.position[0], max.position[1] - min.position[1], max.position[2] - min.position[2]));

    float scale = meshExtent;
    for (int i = 1; i < power; ++i)
        scale = qMin(scale * scale, sqrtf(scale));

    //scale = qLn(meshExtent);
    scale = scale / divisor;

    // Translate the vertices
    qsrand(QTime::currentTime().msec());
    for (int i = 0; i < positions.size(); ++i)
    {
        // Random weight
        float weight = randFloat(0.0f, 1.0f) * scale;

        positions[i] = positions[i] + normals[i] * weight;
    }

    // Change to the new geometry
    m_levels.push_back(new Geometry(positions, geometry->getIndices()));
    m_currentLevel = m_levels.size() - 1;
}

void Mesh::setLevel(int level)
{
    if (level >= 0 && level < m_levels.size())
    {
        m_currentLevel = level;
    }
}

int Mesh::getLevels() const
{
    return m_levels.size();
}

int Mesh::getCurrentLevel() const
{
    return m_currentLevel;
}

Geometry* Mesh::getCurrentGeometry() const
{
    return m_currentLevel == -1 ? nullptr : m_levels[m_currentLevel];
}

void Mesh::clearLevels(bool clearAll)
{
    int firstId = clearAll ? 0 : getCurrentLevel() + 1;
    for (int i = firstId; i < m_levels.size(); ++i)
        delete m_levels[i];

    m_levels.resize(firstId);
}

void Mesh::extractMesh(Qt3DRender::QGeometryRenderer* geometryRenderer)
{
    // Clear the undo history first
    clearLevels(false);

    // Extract the geometry we are copying
    Qt3DRender::QGeometry* geometry = geometryRenderer->geometry();
    if (geometry == nullptr)
        geometry = (*geometryRenderer->geometryFactory())();

    // Store it as the base level
    m_levels.push_back(new Geometry(geometry));

    // Also store it in the qobject
    m_currentLevel = m_levels.size() - 1;
}
