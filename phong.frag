#version 330

uniform vec3 eyePosition;

const int MAX_LIGHTS = 8;
const int TYPE_POINT = 0;
const int TYPE_DIRECTIONAL = 1;
const int TYPE_SPOT = 2;
struct Light
{
    int type;
    vec3 position;
    vec3 color;
    float intensity;
    vec3 direction;
    vec3 attenuation;
    float cutOffAngle;
};

uniform Light lights[MAX_LIGHTS];
uniform int lightCount;

in vec3 position;
in vec3 normal;
in vec4 vertColor;

out vec4 outColor;

vec3 calcLight(vec3 position, vec3 normal)
{
    // Accumulated values
    vec3 diffuseColor = vec3(0.0);
    vec3 specularColor = vec3(0.0);

    // Go through each light source and process it
    for (int i = 0; i < lightCount; ++i)
    {
        // Direction of the light
        vec3 incidence = normalize(lights[i].direction);

        // Computed diffuse intensity
        float diffuseIntensity = max(dot(-incidence, normal), 0.0);

        // Accumulate the computed diffuse intensity
        diffuseColor += diffuseIntensity * lights[i].intensity * lights[i].color;

        // Vector pointing from the vertex to the eye position
        vec3 toEye = normalize(eyePosition - position);

        // Incident light direction reflected by the surface normal
        vec3 reflected = reflect(incidence, normal);

        // Computed specular intensity
        float specularIntensity = pow(max(dot(toEye, reflected), 0.0), 255);

        // Accumulate the computed specular intensity
        specularColor += specularIntensity * lights[i].intensity * lights[i].color;
    }

    // Return the accumulated diffuse color
    return diffuseColor + specularColor;
}

void main(void)
{
    outColor = vec4(calcLight(position, normalize(normal)), 1) * vertColor;
}
