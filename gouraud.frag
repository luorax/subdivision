#version 330

in vec3 lightColor;
in vec4 vertColor;

out vec4 outColor;

void main(void)
{
    outColor = vec4(lightColor, 1) * vertColor;
}
